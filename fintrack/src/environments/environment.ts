// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  apiBaseUrl: 'http://127.0.0.1:8000/api/',

  apiSelfEndpoint: 'self',
  authenticationEndpoint: 'authenticate',
  getCategoryEndpoint: 'category/',
  modifyCategoryEndpoint: 'category/',
  apiCategoryListEndpoint: 'categories/list',
  apiOwnCategoryListEndpoint: 'categories/list/own',
  apiCategoryTreeEndpoint: 'categories/tree',
  apiIndexedCategoryListEndpoint: 'categories/indexed',
  apiEntryCreateEndpoint: 'entry',
  apiEntryDeleteEndpoint: 'entry/',
  apiGetLastNEntriesEndpoint: 'entries/last/',
  apiGetEntriesEndpoint: 'entries',
  apiGetEntryEndpoint: 'entry/',
  apiEntryPatchEndpoint: 'entry/',
  apiGetTagStatisticsEndpoint: 'statistics/tags',
  apiGetCategoryStatisticsEndpoint: 'statistics/categories',
  deficitStatisticsEndpoint:'statistics/deficit',
  rangedDeficitStatisticsEndpoint:'statistics/deficit/{from}/{to}',

  apiCategoryCreateEndpoint: 'category',
  importCsvEndpoint: 'csv/entries',


  storageBearerToken: 'ftBearerToken',
  storageAccountName: 'ftAccountName',
  storageAccountEmail: 'ftAccountEmail'

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
