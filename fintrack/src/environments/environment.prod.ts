export const environment = {
  production: true,

  apiBaseUrl: 'https://fintrack-api.justin-guth.de/api/',

  apiSelfEndpoint: 'self',
  authenticationEndpoint: 'authenticate',
  getCategoryEndpoint: 'category/',
  modifyCategoryEndpoint: 'category/',
  apiCategoryListEndpoint: 'categories/list',
  apiOwnCategoryListEndpoint: 'categories/list/own',
  apiCategoryTreeEndpoint: 'categories/tree',
  apiIndexedCategoryListEndpoint: 'categories/indexed',
  apiEntryCreateEndpoint: 'entry',
  apiEntryDeleteEndpoint: 'entry/',
  apiGetLastNEntriesEndpoint: 'entries/last/',
  apiGetEntriesEndpoint: 'entries',
  apiGetEntryEndpoint: 'entry/',
  apiEntryPatchEndpoint: 'entry/',
  apiGetTagStatisticsEndpoint: 'statistics/tags',
  apiGetCategoryStatisticsEndpoint: 'statistics/categories',
  deficitStatisticsEndpoint:'statistics/deficit',
  rangedDeficitStatisticsEndpoint:'statistics/deficit/{from}/{to}',
  apiCategoryCreateEndpoint: 'category',
  importCsvEndpoint: 'csv/entries',


  storageBearerToken: 'ftBearerToken',
  storageAccountName: 'ftAccountName',
  storageAccountEmail: 'ftAccountEmail'
};
