import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserLoginGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const url: string = state.url;

    console.log(url);
    return this.checkLogin(url);
  }

  checkLogin(url: string): true | UrlTree {

    const isUserLoggedIn: boolean = this.authService.isUserLoggedIn();


    if (!isUserLoggedIn) {
      console.log('User is not logged in. Redirecting to login page.');
    }

    if (isUserLoggedIn) {
      console.log('User is logged in.');
      if (url === '/login') {

        console.log('User is trying to access /login route. Redirecting to /dashboard.');
        return this.router.parseUrl('/dashboard');
      }
      else {

        return true;
      }
    } else {
      if (url === '/login') {
        return true;
      } else {

        return this.router.parseUrl('/login');
      }
    }
  }
}
