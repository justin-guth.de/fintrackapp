import { Tag } from './../types/tag';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TagReplacerService {

  constructor() { }

  public replaceTags(text:string, tags: Tag[]) : string {

    if (text == null ||text == undefined) {

      return null;
    }

    let result = text;

    tags.forEach((tag: Tag) => {

      let replaced = tag.tag.replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");
      result = result.replace(new RegExp(tag.tag + "(\\s|$)", "g"), "<span class=\"text_tag\" style=\"color: " + tag.color + ";\">"+ replaced + "</span> ");
    })

    return result;
  }
}
