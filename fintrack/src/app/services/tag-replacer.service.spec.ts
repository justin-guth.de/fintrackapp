import { TestBed } from '@angular/core/testing';

import { TagReplacerService } from './tag-replacer.service';

describe('TagReplacerService', () => {
  let service: TagReplacerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TagReplacerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
