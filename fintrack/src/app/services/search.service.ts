import { Injectable } from '@angular/core';
import { Entry } from '../types/entry';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  constructor() { }

  matches(entry: Entry, searchPattern: string): unknown {

    const regex: RegExp = new RegExp('.*' + searchPattern.toLowerCase().replace(/[.*+?^${}()|[\]\\]/g, '\\$&') + '.*', 'g');

    return (entry.title?.toLowerCase().match(regex) != null)
      || (entry.description?.toLowerCase().match(regex) != null)
      || (entry.categoryName.toLowerCase().match(regex) != null);
  }
}
