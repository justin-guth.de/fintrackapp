import { TestBed } from '@angular/core/testing';

import { DeficitService } from './deficit.service';

describe('DeficitService', () => {
  let service: DeficitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeficitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
