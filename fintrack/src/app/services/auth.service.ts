import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AccountService } from './account.service';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private bearerToken: string;

  constructor(private httpService: HttpService, private accountService: AccountService, private router : Router) {

    this.checkToken();
  }

  login(email: string, password: string) {

    console.log("Logging in: " + email + ".");

    const parameters = {
      email: email,
      password: password,
      device_name: "fintrackapp",
      remember: false
    };


    this.httpService.post(
      environment.apiBaseUrl + environment.authenticationEndpoint,
      parameters
    ).subscribe(
      (data: object) => {

        if (!data["is_error"]) {
          this.bearerToken= data["data"]["token"];
        }

        console.log("Logged in? : " + this.isUserLoggedIn());
        localStorage.setItem(environment.storageBearerToken, this.bearerToken);

        this.accountService.refresh(() => {
          this.router.navigate(["dashboard"]);
        });
      }
    )
  }

  logout() {

    this.bearerToken = null;
    localStorage.setItem(environment.storageBearerToken, null);

  }

  checkToken() {

    this.bearerToken = localStorage.getItem(environment.storageBearerToken);

    console.log("Checking token.");

    if (this.bearerToken != null) {

      const parameters = {
        headers: { Authorization: "Bearer " + this.bearerToken }
      };

      this.httpService.get(
        environment.apiBaseUrl + environment.apiSelfEndpoint,
        parameters
      ).subscribe(
        (data: object) => {

          if(data["is_error"]) {

            this.bearerToken = null;
            localStorage.setItem(environment.storageBearerToken, null);
          };
        }
      )
    }

  }

  isUserLoggedIn(): boolean {

    return this.bearerToken != null;
  }

}
