import { EntryService } from 'src/app/services/entry.service';
import { ApiResponse } from './../types/api-response';
import { CreatedCategory } from './../types/created-category';
import { CountedCategory } from './../types/counted-category';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Category } from '../types/category';
import { CategoryTreeNode } from '../types/category-tree-node';
import { AuthHttpService } from './auth-http.service';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private authHttp: AuthHttpService) { }

  modifyCategory(id: string, name: string, color: string, parentId: string, icon: string, then: (category: Category) => void) {

    this.authHttp.patchAuth(environment.apiBaseUrl + environment.modifyCategoryEndpoint + id, {
      name,
      color,
      parent_id: parentId,
      icon
    }).subscribe((response: ApiResponse) => {

      EntryService.notifyOnChangeCallbacks();

      if (response.is_error) {
        console.error(response);
        then(null);
      }
      else {
        then(response.data as Category);
      }
    });
  }

  getCategory(id: string, then: (c: Category) => void) {

    this.authHttp.getAuth(environment.apiBaseUrl + environment.getCategoryEndpoint + id).subscribe(
      (response: ApiResponse) => {
        if (response["is_error"]) {
          console.error(response);
          then(null);
        }
        else {
          then(response.data as Category);
        }
      });
  }

  /*
  getOwnCategories(then: (categories: Category[]) => void) {

    this.authHttp.getAuth(environment.apiBaseUrl + environment.apiOwnCategoryListEndpoint).subscribe(
      (data) => {
        if (data["is_error"]) {
          console.error(data);
          then(null);
        }
        else {

          let resultingList = data["data"].map((element) => {

            var result: Category = {

              id: element["id"],
              name: element["name"],
              parent_category_id: element["parent_category_id"],
              is_incoming: element["is_incoming"],
              identifier: element["identifier"],
              color: element["color"],
              icon: element["icon"]

            };

            return result;
          });

          // console.log(resultingList);
          then(resultingList);
        }
      }
    )
  } */

  getCategories(then: (categories: Category[]) => void) {

    this.authHttp.getAuth(environment.apiBaseUrl + environment.apiCategoryListEndpoint).subscribe(
      (data) => {
        if (data["is_error"]) {
          console.error(data);
        }

        let resultingList = data["data"].map((element) => {

          var result: Category = {

            id: element["id"],
            name: element["name"],
            parent_id: element["parent_id"],
            is_incoming: element["is_incoming"],
            color: element["color"],
            icon: element["icon"]

          };

          return result;
        });

        // console.log(resultingList);
        then(resultingList);
      }
    )
  }

  getIndexedCategories(then: (categories: Category[]) => void) {

    this.authHttp.getAuth(environment.apiBaseUrl + environment.apiIndexedCategoryListEndpoint).subscribe(
      (data) => {
        if (data["is_error"]) {
          console.error(data);
        }

        let resultingList = data["data"];

        // console.log(resultingList);
        then(resultingList);
      }
    )
  }

  getCategoryTree(then: (tree: { outgoing: CategoryTreeNode; incoming: CategoryTreeNode }) => void) {

    this.authHttp.getAuth(environment.apiBaseUrl + environment.apiCategoryTreeEndpoint).subscribe(
      (data) => {
        if (data["is_error"]) {
          console.error(data);
        }

        let result: { outgoing: CategoryTreeNode, incoming: CategoryTreeNode } = {

          outgoing: null,
          incoming: null
        };

        let element0 = data["data"][0];
        let element1 = data["data"][1];

        if (element0["category"]["is_incoming"]) {

          result.incoming = CategoryTreeNode.fromArrayTree(element0);
          result.outgoing = CategoryTreeNode.fromArrayTree(element1);
        }
        else {

          result.incoming = CategoryTreeNode.fromArrayTree(element1);
          result.outgoing = CategoryTreeNode.fromArrayTree(element0);
        }

        // console.log(data);
        // console.log("Resulting Tree:", result);

        then(result);
      });
  }

  getCategoriesFromTree(then: (categories: { outgoing: Category[], incoming: Category[] }) => void) {

    this.authHttp.getAuth(environment.apiBaseUrl + environment.apiCategoryTreeEndpoint).subscribe(
      (data) => {
        if (data["is_error"]) {
          console.error(data);
        }

        var result: object[] = [];

        var flatten = (tree: object, depth = 0) => {

          (tree as object[]).forEach(
            (child) => {

              let appended = child["category"];
              appended._meta = { depth: depth };
              result.push(appended);
              result.concat(flatten(child["children"], depth + 1));
            }
          );

          return result;
        }

        let resultingList = flatten(data["data"]).map((category) => {
          let result: Category = {
            id: category["id"],
            name: category["name"],
            parent_id: category["parent_id"],
            is_incoming: category["is_incoming"],
            _meta: category["_meta"],
            color: category["color"],
            icon: category["icon"]
          };

          return result;
        });

        let resultingLists = {
          incoming: resultingList.filter((element) => { return element.is_incoming }),
          outgoing: resultingList.filter((element) => { return !element.is_incoming })
        };

        then(resultingLists);
      }
    )
  }

  createCategory(category: CreatedCategory, then: (category: CreatedCategory) => void) {

    this.authHttp.postAuth(environment.apiBaseUrl + environment.apiCategoryCreateEndpoint, category).subscribe((response: ApiResponse) => {

      if (response.is_error) {
        console.error(response);
        then(null);
      }

      else {
        then(response.data as CreatedCategory);
      }
    });
  }

  getCategoryStatistics(then: (categories: CountedCategory[]) => void) {

    this.authHttp.getAuth(environment.apiBaseUrl + environment.apiGetCategoryStatisticsEndpoint).subscribe((response) => {

      if (response["is_error"]) {

        then(null);
      }

      then(response["data"]);
    });
  }
}
