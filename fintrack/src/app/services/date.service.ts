import { DateRange } from './../types/date-range';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DateService {
  constructor() {}

  isInOrder(date1: string, date2: string): boolean {
    return new Date(date2) >= new Date(date1);
  }

  isInBounds(begin: string, value: string, end: string) {
    return this.isInOrder(begin, value) && this.isInOrder(value, end);
  }

  normaliseDate(date: string) {
    return date.substr(0, 10);
  }

  getMonthRange(date: Date): DateRange {
    const year = date.getFullYear();
    const month = date.getMonth();

    const firstDay = new Date(year, month, 1);

    const firstDayNextMonth = new Date(
      year + (month === 11 ? 1 : 0),
      (month + 1) % 12,
      1
    );


    const lastDay = new Date(firstDayNextMonth.getTime());
    lastDay.setDate(firstDayNextMonth.getDate() - 1);


    return {
      from: this.normalise(firstDay),
      to: this.normalise(lastDay),
    };
  }

  getNormalised() {
    const today = new Date();
    return this.normalise(today);
  }

  normalise(date: Date) {
    const timeZoneDifference = (date.getTimezoneOffset() / 60) * -1;
    date.setTime(date.getTime() + timeZoneDifference * 60 * 60 * 1000);
    return date;
  }

  getLastMonthRange(date: Date): DateRange {
    const year = date.getFullYear();
    const month = date.getMonth();

    const fistDayNextMonth = new Date(
      year - (month === 0 ? 1 : 0),
      (month - 1) % 12,
      1
    );

    return this.getMonthRange(fistDayNextMonth);
  }

  getYearRange(date: Date): DateRange {
    const year = date.getFullYear();

    const firstDay = new Date(year, 0, 1);

    const fistDayNextYear = new Date(year + 1, 0, 1);

    const lastDay = new Date(fistDayNextYear.getTime());
    lastDay.setDate(fistDayNextYear.getDate() - 1);

    return {
      from: this.normalise(firstDay),
      to: this.normalise(lastDay),
    };
  }

  getLastYearRange(date: Date): DateRange {
    const year = date.getFullYear();

    const fistDayNextYear = new Date(year - 1, 0, 1);

    return this.getYearRange(fistDayNextYear);
  }
}
