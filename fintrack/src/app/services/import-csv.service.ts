import { EntryService } from 'src/app/services/entry.service';
import { Entry } from './../types/entry';
import { ApiResponse } from './../types/api-response';
import { environment } from './../../environments/environment';
import { AuthHttpService } from './auth-http.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImportCsvService {

  constructor(private authHttp: AuthHttpService) { }

  public importCsv(fileContent: string, then: (results: Entry[]) => void) {

    this.authHttp.postAuth(environment.apiBaseUrl + environment.importCsvEndpoint, {
      data: fileContent
    }).subscribe((response: ApiResponse) => {

      if (response.is_error) {
        console.error(response);
        then(null);
      }
      else{

        EntryService.notifyOnChangeCallbacks();

        then(response.data);
      }


    });
  }
}
