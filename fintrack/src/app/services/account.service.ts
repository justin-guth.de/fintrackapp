import { Injectable } from '@angular/core';
import { IonRefresher } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { AuthHttpService } from './auth-http.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private authHttpService: AuthHttpService) {

    this.refresh();
  }

  getAccountName(then: (name: string) => void) {

    var current = localStorage.getItem(environment.storageAccountName);

    if (current == null) {
      this.refresh(() => {
        then(localStorage.getItem(environment.storageAccountName));
      });
    }
    else {
      then(current);
    }
  }

  getAccountEmail() {

    return localStorage.getItem(environment.storageAccountEmail);
  }

  refresh(then?: () => void) {

    this.authHttpService.getAuth(environment.apiBaseUrl + environment.apiSelfEndpoint)
      .subscribe((data) => {
        if (!data["is_error"]) {
          localStorage.setItem(environment.storageAccountEmail, data["data"]["email"]);
          localStorage.setItem(environment.storageAccountName, data["data"]["name"]);
        }

        if (then != null) {
          then();
        }
      });
  }
}
