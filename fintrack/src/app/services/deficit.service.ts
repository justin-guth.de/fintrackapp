import { ApiResponse } from './../types/api-response';
import { environment } from 'src/environments/environment';
import { AuthHttpService } from './auth-http.service';
import { Injectable } from '@angular/core';
import { Deficit } from '../types/deficit';

@Injectable({
  providedIn: 'root',
})
export class DeficitService {
  constructor(private authHttp: AuthHttpService) {}

  public getDeficitInRange(
    beginDate: string,
    endDate: string,
    then: (deficit: Deficit) => void
  ) {
    this.authHttp
      .getAuth(
        environment.apiBaseUrl +
          environment.rangedDeficitStatisticsEndpoint
            .replace('{from}', beginDate)
            .replace('{to}', endDate)
      )
      .subscribe((response: ApiResponse) => {
        if (response.is_error) {
          console.error(response);
          then(null);
        } else {
          then(response.data);
        }
      });
  }

  public getDeficit(then: (deficit: Deficit) => void) {
    this.authHttp
      .getAuth(environment.apiBaseUrl + environment.deficitStatisticsEndpoint)
      .subscribe((response: ApiResponse) => {
        if (response.is_error) {
          console.error(response);
          then(null);
        } else {
          then(response.data);
        }
      });
  }
}
