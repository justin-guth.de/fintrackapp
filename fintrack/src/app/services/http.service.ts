import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  post(url: string, body: object, options?: object) {

    return this.http.post(url, body, options);
  }

  get(url: string, options?: object) {

    return this.http.get(url, options);
  }
}
