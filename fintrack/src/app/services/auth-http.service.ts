import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthHttpService {

  constructor(private http: HttpClient) { }

  patchAuth(url: string, body: any) {

    if(localStorage.getItem(environment.storageBearerToken) == null) {

      console.error("Cannot call auth http methods if user is not logged in.");
    }

    const parameters = {
      headers: { Authorization: "Bearer " + localStorage.getItem(environment.storageBearerToken) }
    };

    return this.http.patch(url, body, parameters);
  }


  getAuth(url: string) {

    if(localStorage.getItem(environment.storageBearerToken) == null) {

      console.error("Cannot call auth http methods if user is not logged in.");
    }

    const parameters = {
      headers: { Authorization: "Bearer " + localStorage.getItem(environment.storageBearerToken) }
    };

    return this.http.get(url, parameters);
  }

  deleteAuth(url: string) {

    if(localStorage.getItem(environment.storageBearerToken) == null) {

      console.error("Cannot call auth http methods if user is not logged in.");
    }

    const parameters = {
      headers: { Authorization: "Bearer " + localStorage.getItem(environment.storageBearerToken) }
    };

    return this.http.delete(url, parameters);
  }

  postAuth(url: string, body?: object) {

    if(localStorage.getItem(environment.storageBearerToken) == null) {

      console.error("Cannot call auth http methods if user is not logged in.");
    }

    const parameters = {
      headers: { Authorization: "Bearer " + localStorage.getItem(environment.storageBearerToken) }
    };

    return this.http.post(url, body, parameters);
  }


}
