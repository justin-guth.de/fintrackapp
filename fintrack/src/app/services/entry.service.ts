import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Entry } from '../types/entry';
import { AuthHttpService } from './auth-http.service';

@Injectable({
  providedIn: 'root'
})
export class EntryService {

  private static _onChangeCallbacks: (() => void)[] = [];

  updateEntry(id: string, title: string, amount: number, category_id: string, description: string, date: string, then: (any) => void) {

    this.authHttp.patchAuth(environment.apiBaseUrl + environment.apiEntryPatchEndpoint + id,
      {
        title: title,
        amount: amount,
        category_id: category_id,
        description: description,
        date: date
      }).subscribe((response) => {

        if (response["is_error"]) {
          console.error(response);
        }

        then(response);
        EntryService.notifyOnChangeCallbacks();
      });
  }

  public static notifyOnChangeCallbacks() {

    EntryService._onChangeCallbacks.forEach(func => {

      // console.log("Calling callback:", func);
      func();
    });
  }

  addOnChangeCallback(func: () => void) {

    EntryService._onChangeCallbacks.push(func);
  }

  deleteEntry(id: string, then: (response: any) => void) {

    this.authHttp.deleteAuth(environment.apiBaseUrl + environment.apiEntryDeleteEndpoint + id).subscribe((response) => {

      if (response["is_error"]) {
        console.error(response);
      }

      then(response);

      EntryService._onChangeCallbacks.forEach(func => {

        // console.log("Calling callback:", func);
        func();
      });
    });
  }

  constructor(private authHttp: AuthHttpService) { }

  createEntry(entry: Entry, then: (response: object) => void) {

    this.authHttp.postAuth(environment.apiBaseUrl + environment.apiEntryCreateEndpoint, entry).subscribe((response) => {

      if (response["is_error"]) {
        console.error(response);
      }

      then(response);
    });
  }

  getLastNEntries(n: number, then: (response: Entry[]) => void) {

    this.authHttp.getAuth(environment.apiBaseUrl + environment.apiGetLastNEntriesEndpoint + n).subscribe((response) => {

      if (response["is_error"]) {
        console.error(response);
      }

      then(response["data"].map((entry) => {
        let result: Entry = {
          id: entry["id"],
          title: entry["title"],
          description: entry["description"],
          date: entry["date"],
          amount: entry["amount"],
          absolute_amount: entry["absolute_amount"],
          category_id: entry["category_id"],
          amountRepresentation: entry["amount_representation"],
          categoryName: entry["category_name"],
          categoryColor: entry["category_color"],
          tags: entry["tags"],
          categoryIcon: entry["category_icon"]
        }
        return result;
      }));
    })
  }

  getEntries(then: (response: Entry[]) => void) {

    this.authHttp.getAuth(environment.apiBaseUrl + environment.apiGetEntriesEndpoint).subscribe((response) => {

      if (response["is_error"]) {
        console.error(response);
      }

      then(response["data"].map((entry) => {

        let result: Entry = {
          id: entry["id"],
          title: entry["title"],
          description: entry["description"],
          date: entry["date"],
          absolute_amount: entry["absolute_amount"],
          amount: entry["amount"],
          category_id: entry["category_id"],
          amountRepresentation: entry["amount_representation"],
          categoryName: entry["category_name"],
          categoryColor: entry["category_color"],
          tags: entry["tags"],
          categoryIcon: entry["category_icon"]
        }
        return result;
      }));
    })
  }

  getEntry(id: string, then: (response: Entry) => void) {

    this.authHttp.getAuth(environment.apiBaseUrl + environment.apiGetEntryEndpoint + id).subscribe((response) => {

      if (response["is_error"]) {
        console.error(response);
      }

      const entry = response["data"];
      then(
        {
          id: entry["id"],
          title: entry["title"],
          description: entry["description"],
          date: entry["date"],
          amount: entry["amount"],
          absolute_amount: entry["absolute_amount"],
          category_id: entry["category_id"],
          amountRepresentation: entry["amount_representation"],
          categoryName: entry["category_name"],
          categoryColor: entry["category_color"],
          tags: entry["tags"],
          categoryIcon: entry["category_icon"],
        }
      );
    });
  }
}
