import { environment } from '../../environments/environment';
import { AuthHttpService } from './auth-http.service';
import { CountedTag } from '../types/counted-tag';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private authHttp: AuthHttpService) { }

  getTagStatistics(then: (tags: CountedTag[]) => void) {

    this.authHttp.getAuth(environment.apiBaseUrl + environment.apiGetTagStatisticsEndpoint).subscribe((response) => {

      if (response["is_error"]) {

        then(null);
      }

      then(response["data"]);
    });
  }
}
