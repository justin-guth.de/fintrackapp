import { TestBed } from '@angular/core/testing';

import { CurrentPlatformServiceService } from './current-platform-service.service';

describe('CurrentPlatformServiceService', () => {
  let service: CurrentPlatformServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CurrentPlatformServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
