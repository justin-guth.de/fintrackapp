import { VerticalColorBarComponent } from './../../components/vertical-color-bar/vertical-color-bar.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [VerticalColorBarComponent],
  exports: [VerticalColorBarComponent],
  imports: [
    CommonModule
  ]
})
export class VerticalColorBarModule { }
