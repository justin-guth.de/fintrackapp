import { IonicModule } from '@ionic/angular';
import { TinyChipComponent } from './../../components/tiny-chip/tiny-chip.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [TinyChipComponent],
  exports: [TinyChipComponent],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class TinyChipModule { }
