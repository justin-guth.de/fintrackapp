import { SafeHtmlPipe } from './../../pipes/safe-html.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [SafeHtmlPipe],
  exports: [SafeHtmlPipe],
  imports: [
    CommonModule
  ]
})
export class SafeHtmlModule { }
