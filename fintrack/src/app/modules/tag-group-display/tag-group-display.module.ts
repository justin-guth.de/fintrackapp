import { AmountNumberDisplayModule } from './../amount-number-display/amount-number-display.module';
import { CollapsibleCardModule } from './../collapsible-card/collapsible-card.module';
import { TagBoxModule } from './../tag-box/tag-box.module';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { TagGroupDisplayComponent } from './../../components/tag-group-display/tag-group-display.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [TagGroupDisplayComponent],
  exports: [TagGroupDisplayComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TagBoxModule,
    CollapsibleCardModule,
    AmountNumberDisplayModule
  ]
})
export class TagGroupDisplayModule { }
