import { AmountNumberDisplayComponent } from './../../components/amount-number-display/amount-number-display.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [AmountNumberDisplayComponent],
  exports: [AmountNumberDisplayComponent],
  imports: [
    CommonModule
  ]
})
export class AmountNumberDisplayModule { }
