import { CategoryBoxModule } from './../category-box/category-box.module';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CategorySelectionComponent } from './../../components/category-selection/category-selection.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [CategorySelectionComponent],
  exports: [CategorySelectionComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    CategoryBoxModule
  ]
})
export class CategorySelectionModule { }
