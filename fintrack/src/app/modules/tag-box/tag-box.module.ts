import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagBoxComponent } from 'src/app/components/tag-box/tag-box.component';



@NgModule({
  declarations: [TagBoxComponent],
  exports: [TagBoxComponent],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class TagBoxModule { }
