import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CategoryBoxComponent } from './../../components/category-box/category-box.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [CategoryBoxComponent],
  exports: [CategoryBoxComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ]
})
export class CategoryBoxModule { }
