import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CollapsibleCardComponent } from '../../components/collapsable-card/collapsible-card.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [CollapsibleCardComponent],
  exports: [CollapsibleCardComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ]
})
export class CollapsibleCardModule { }
