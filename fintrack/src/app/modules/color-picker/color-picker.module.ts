import { IonicModule } from '@ionic/angular';
import { ColorPickerComponent } from './../../components/color-picker/color-picker.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [ColorPickerComponent],
  exports: [ColorPickerComponent],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class ColorPickerModule { }
