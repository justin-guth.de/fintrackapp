import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { HiddenListingAreaComponent } from './../../components/hidden-listing-area/hidden-listing-area.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [HiddenListingAreaComponent],
  exports: [HiddenListingAreaComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ]
})
export class HiddenListingAreaModule { }
