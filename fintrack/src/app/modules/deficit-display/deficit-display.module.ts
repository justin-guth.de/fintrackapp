import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DeficitDisplayComponent } from './../../components/deficit-display/deficit-display.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [DeficitDisplayComponent],
  exports: [DeficitDisplayComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ]
})
export class DeficitDisplayModule { }
