import { AmountNumberDisplayModule } from './../amount-number-display/amount-number-display.module';
import { CollapsibleCardModule } from './../collapsible-card/collapsible-card.module';
import { VerticalColorBarModule } from './../vertical-color-bar/vertical-color-bar.module';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryStatisticsComponent } from 'src/app/components/category-statistics/category-statistics.component';



@NgModule({
  declarations: [CategoryStatisticsComponent],
  exports: [CategoryStatisticsComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    VerticalColorBarModule,
    CollapsibleCardModule,
    AmountNumberDisplayModule
  ]
})
export class CategoryStatisticsModule { }
