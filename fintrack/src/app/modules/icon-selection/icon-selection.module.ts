import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconSelectionComponent } from 'src/app/components/icon-selection/icon-selection.component';



@NgModule({
  declarations: [IconSelectionComponent],
  exports: [IconSelectionComponent],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class IconSelectionModule { }
