import { FormsModule } from '@angular/forms';
import { DashboardPageRoutingModule } from '../../components/dashboard/dashboard-routing.module';
import { AmountInputComponent } from '../../components/amount-input/amount-input.component';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [AmountInputComponent],
  exports: [AmountInputComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule
  ]
})
export class AmountInputModule {}
