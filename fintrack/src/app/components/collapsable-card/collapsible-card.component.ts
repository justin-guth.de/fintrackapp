import { Component, Input, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { IonCardContent, IonButton } from '@ionic/angular';

@Component({
  selector: 'app-collapsible-card',
  templateUrl: './collapsible-card.component.html',
  styleUrls: ['./collapsible-card.component.scss'],
})
export class CollapsibleCardComponent implements OnInit, AfterViewInit {

  private static counter = 0;
  @Input() title: string;

  public id;
  public expanded: boolean = false;
  public buttonText: string = 'Expand';

  private contentElement: HTMLElement;

  constructor() {

    this.id = CollapsibleCardComponent.counter;
    CollapsibleCardComponent.counter++;
  }

  ngOnInit() { }

  ngAfterViewInit() {

    this.contentElement = document.querySelector('#collapsibleContent_' + this.id);
  }

  toggle() {

    this.contentElement.classList.toggle('expanded');
    this.expanded = !this.expanded;
    this.buttonText = this.expanded ? 'Collapse' : 'Expand';
  }

}
