import { Tag } from './../../types/tag';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tag-box',
  templateUrl: './tag-box.component.html',
  styleUrls: ['./tag-box.component.scss'],
})
export class TagBoxComponent implements OnInit {

  @Input() tag: Tag;

  constructor() { }

  ngOnInit() {}

}
