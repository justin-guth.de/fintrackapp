import { splitTypescriptSuffix } from '@angular/compiler/src/aot/util';
import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { IonSearchbar, IonSegment } from '@ionic/angular';
import ionicons from '../../../assets/ionicons.json';

@Component({
  selector: 'app-icon-selection',
  templateUrl: './icon-selection.component.html',
  styleUrls: ['./icon-selection.component.scss'],
})
export class IconSelectionComponent implements OnInit {

  @Input() value: string;
  @ViewChild('searchBar') searchBar: IonSearchbar;
  @ViewChild('styleSelector') styleSelector: IonSegment;
  @Output() iconSelected: EventEmitter<string> = new EventEmitter<string>();

  public icons = null;
  public currentStyle: string;

  private defaultIcons = ionicons.icons.filter((icon) =>
    !(icon.name.endsWith('-outline') || icon.name.endsWith('-sharp'))
  );

  private currentSelection = this.defaultIcons;

  private searchIndex = {};


  constructor() {
    this.icons = this.currentSelection;
    this.currentStyle = '';
  }

  ngOnInit() {

  }

  search() {

    const text: string = this.searchBar.value;

    if (text == null || text === '') {
      this.icons = this.currentSelection;
    }

    if (this.searchIndex[text] != null && this.searchIndex[text] !== undefined) {

      this.icons = this.searchIndex[text];
      return;
    }


    const regex = new RegExp('.*' + text.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') + '.*', 'g');

    this.searchIndex[text] = this.defaultIcons.filter(
      (icon) => {

        if (icon.name.search(regex) !== -1) {
          return true;
        };

        for (const tag of icon.tags) {

          if (tag.search(regex) !== -1) {

            return true;
          };
        }

        return false;
      }
    );

    this.icons = this.searchIndex[text];
  }

  select(name) {

    this.value = name;

    this.iconSelected.emit(name);
  }

  selectStyle() {

    const style = this.styleSelector.value;

    if (style === 'default') {

      this.currentStyle = '';
    } else if (style === 'outline') {
      this.currentStyle = '-outline';
    }
    else {
      this.currentStyle = '-sharp';
    }


    this.search();
  }
}
