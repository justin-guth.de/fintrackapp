import { CategorySelectionModule } from './../../modules/category-selection/category-selection.module';
import { EntryListItemModuleModule } from './../../entry-list-item-module/entry-list-item-module.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardPageRoutingModule } from './dashboard-routing.module';

import { DashboardPage } from './dashboard.page';
import { CreateEntryComponent } from '../create-entry/create-entry.component';
import { ShortEntryListComponent } from '../short-entry-list/short-entry-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule,
    EntryListItemModuleModule,
    CategorySelectionModule
  ],
  declarations: [DashboardPage, CreateEntryComponent, ShortEntryListComponent]
})
export class DashboardPageModule {}
