import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';
import { ShortEntryListComponent } from '../short-entry-list/short-entry-list.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit, AfterViewInit {


  @ViewChild('shortEntryList') shortEntryList;
  @ViewChild('createEntry') createEntry;

  public userName: string;

  constructor(private accountService: AccountService) {


  }

  onEntryCreated() {

    if (!this.shortEntryList) { return; }

    this.shortEntryList.reloadEntries();
  }

  ngOnInit() {

    this.accountService.getAccountName((name) => {
      this.userName = name;
    });

  }

  ngAfterViewInit() {

    this.createEntry.addOnEntryCreated(() => {

      this.onEntryCreated();
    });
  }

}
