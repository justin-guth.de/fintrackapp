import { TagBoxModule } from './../../modules/tag-box/tag-box.module';
import { SafeHtmlModule } from './../../modules/safe-html/safe-html.module';
import { EntryDetailsComponent } from './../entry-details/entry-details.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsPagePageRoutingModule } from './details-page-routing.module';

import { DetailsPagePage } from './details-page.page';

@NgModule({
  imports: [
    SafeHtmlModule,
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsPagePageRoutingModule,
    TagBoxModule
  ],
  declarations: [DetailsPagePage , EntryDetailsComponent]

})
export class DetailsPagePageModule {}
