import { Entry } from 'src/app/types/entry';
import { EntryService } from 'src/app/services/entry.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.page.html',
  styleUrls: ['./details-page.page.scss'],
})
export class DetailsPagePage implements OnInit {

  public entry: Entry;
  private id: string;

  constructor(private entryService: EntryService, private route: ActivatedRoute) {

    this.id = this.route.snapshot.params.id;
    this.entryService.getEntry(this.id, (entry) => {

      this.entry = entry;
    });
  }
  ngOnInit() {

    this.entryService.addOnChangeCallback(() => {

      this.entryService.getEntry(this.id, (entry) => {

        this.entry = entry;
      });
    });
  }

}
