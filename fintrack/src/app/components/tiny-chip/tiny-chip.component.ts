import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tiny-chip',
  templateUrl: './tiny-chip.component.html',
  styleUrls: ['./tiny-chip.component.scss'],
})
export class TinyChipComponent implements OnInit {

  @Input() color: string;
  @Input() icon: string = null;
  @Input() outline: boolean = false;

  constructor() { }

  ngOnInit() {}

}
