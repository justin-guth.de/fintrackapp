import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-amount-number-display',
  templateUrl: './amount-number-display.component.html',
  styleUrls: ['./amount-number-display.component.scss'],
})
export class AmountNumberDisplayComponent implements OnInit {

  @Input() amount: number;

  constructor() { }

  ngOnInit() {}

}
