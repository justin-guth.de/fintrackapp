import { SankeyNode } from 'src/app/types/sankey-node';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hidden-listing-area',
  templateUrl: './hidden-listing-area.component.html',
  styleUrls: ['./hidden-listing-area.component.scss'],
})
export class HiddenListingAreaComponent implements OnInit {

  public nodes: SankeyNode[] = [];

  constructor() { }

  ngOnInit() {}

  add(node: SankeyNode) {

    this.nodes.push(node);
  }

  unhide(node: SankeyNode) {

    node.unhide();
    this.nodes = this.nodes.filter((element: SankeyNode) => element !== node);
  }

}
