import { HSVColor } from './../../types/hsvcolor';
import { ColorPickerRenderer } from './../../types/color-picker-renderer';
import { Component, OnInit, ViewChild, AfterViewInit, Output, Input, OnChanges, SimpleChanges } from '@angular/core';
import { IonRange, Platform } from '@ionic/angular';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss'],
})
export class ColorPickerComponent implements OnInit, AfterViewInit, OnChanges {


  @ViewChild('range') range: IonRange;
  @Output() valueChange: EventEmitter<HSVColor> = new EventEmitter<HSVColor>();
  @Input() color: string;

  public width: number = 150;
  public height: number = 150;

  private colorPickerRenderer: ColorPickerRenderer = new ColorPickerRenderer();
  private colorDisplay: HTMLDivElement;

  private isColorSet: boolean;

  constructor(public platform: Platform) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.color !== null && this.color !== undefined && !this.isColorSet) {

      this.isColorSet = true;
      this.colorPickerRenderer.setColor(this.color);
    }
  }

  setColor(color: string) {

    this.colorPickerRenderer.setColor(color);
  }

  getColor(): string {

    return this.colorPickerRenderer.getRGB().code();
  }

  ngOnInit() {

    this.colorPickerRenderer.addOnChangeListener((color: HSVColor) => this.notify(color));
    this.colorDisplay = document.getElementById('colorDisplay') as HTMLDivElement;
  }

  setValue() {

    this.colorPickerRenderer.setValue(this.range.value as number);
  }

  ngAfterViewInit() {

    this.colorPickerRenderer.init(
      document.getElementById('canvas') as HTMLCanvasElement
    );

    this.notify(new HSVColor(0, 0, 1));
  }

  private notify(color: HSVColor): void {

    if (this.valueChange === null || this.valueChange === undefined) {
      return;
    }

    this.valueChange.emit(color);

    if (this.colorDisplay !== null && this.colorDisplay !== undefined) {

      this.colorDisplay.style.backgroundColor = color.toRGB().code();
    }
  }
}


