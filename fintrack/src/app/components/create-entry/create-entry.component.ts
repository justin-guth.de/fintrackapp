import { ApiResponse } from './../../types/api-response';
import { Router } from '@angular/router';
import { CategorySelectionComponent } from './../category-selection/category-selection.component';
import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';
import { EntryService } from 'src/app/services/entry.service';
import { Entry } from 'src/app/types/entry';

@Component({
  selector: 'app-create-entry',
  templateUrl: './create-entry.component.html',
  styleUrls: ['./create-entry.component.scss'],

  providers: [DatePipe]
})
export class CreateEntryComponent implements OnInit, AfterViewInit {

  @ViewChild('categoryInput') categoryInput: CategorySelectionComponent;
  @ViewChild('newEntrySubmitButton') submitButton: HTMLIonButtonElement;


  public categories;
  public categoriesIn;
  public categoriesOut;
  public currencyDisplayValue = '0,00';

  private dateInput;
  private titleInput;
  private descriptionInput;
  private amountInput;
  private isCreating: boolean = false;

  private onCreateEntryListeners: (() => void)[] = [];

  constructor(
    private datePipe: DatePipe,
    private categoryService: CategoryService,
    private entryService: EntryService,
    private router: Router
  ) { }


  ngOnInit() {

  }

  ngAfterViewInit() {

    this.dateInput = document.getElementById('newEntryDateInput');
    this.titleInput = document.getElementById('newEntryTitleInput');
    this.descriptionInput = document.getElementById('newEntryDescriptionInput');
    this.amountInput = document.getElementById('newEntryAmountInput');

    const today = new Date();
    this.dateInput.value = this.datePipe.transform(today, 'yyyy-MM-dd');

    this.categoryService.getCategoriesFromTree(
      (categories) => {
        this.categories = categories;
        this.categoriesIn = categories.incoming;
        this.categoriesOut = categories.outgoing;
      });
    this.updateInputField(0);
  }


  createEntry() {

    this.isCreating = true;
    this.checkButton();

    const title = this.titleInput.value;
    const description = this.descriptionInput.value;
    // TODO this should probably not be done that way
    // Different browsers produce different date formats.
    const date = (this.dateInput.value as string).substr(0, 10);
    const category = this.categoryInput.getSelection();
    const amount = + this.currencyDisplayValue.replace(/\D/g, '');

    const entry: Entry = {

      title,
      description,
      date,
      // eslint-disable-next-line @typescript-eslint/naming-convention
      category_id: category,
      amount,
      // eslint-disable-next-line @typescript-eslint/naming-convention
      absolute_amount: amount,
      tags: null
    };

    this.entryService.createEntry(entry, (response: ApiResponse) => {

      this.isCreating = false;
      this.checkButton();

      if (!response.is_error) {

        this.clearEntry();

        this.onCreateEntryListeners.forEach((callback: () => void) => {

          callback();
        });
      }
    });
  }

  addOnEntryCreated(callback: () => void) {

    this.onCreateEntryListeners.push(callback);
  }

  clearEntry() {

    this.isCreating = false;
    this.titleInput.value = '';
    this.descriptionInput.value = '';
    const today = new Date();
    this.dateInput.value = this.datePipe.transform(today, 'yyyy-MM-dd');
    this.categoryInput.reset();
    this.submitButton.disabled = true;
    this.currencyDisplayValue = '0,00';
    this.checkButton();
  }

  checkButton() {

    if (this.isCreating) {

      this.submitButton.disabled = true;
      return;
    }

    const title = this.titleInput.value;
    const amount = + this.currencyDisplayValue.replace(/\D/g, '');

    if (title === '' || title === null || title === undefined || amount === 0) {

      this.submitButton.disabled = true;
    }
    else {

      this.submitButton.disabled = false;
    }
  }

  onTitleChange() {

    this.checkButton();
  }


  getValue() {

    return this.currencyDisplayValue.replace(/\D/g, '');
  }


  updateInputField(amount) {

    if (amount < 0) {
      amount = 0;
    }

    let zeroRepeat = 3 - amount.toString().length;
    if (zeroRepeat < 0) {
      zeroRepeat = 0;
    }

    const amountString = '0'.repeat(zeroRepeat) + amount;


    this.currencyDisplayValue = amountString.substring(0, amountString.length - 2)
      + ',' + amountString.substring(amountString.length - 2, amountString.length);

    this.checkButton();
  }

  onInput() {

    setTimeout(() => {
      let input = this.currencyDisplayValue;
      input = input.replace(/\D/g, '');
      const amount = + input;

      this.updateInputField(amount);
    }, 1);
  }

  addToValue(n) {

    let nNumber = + (n[0] + n.replace(/\D/g, ''));
    nNumber += +this.currencyDisplayValue.replace(/\D/g, '');
    this.updateInputField(nNumber);
  }

  navigateCsvImport() {

    this.router.navigate(['import-csv']);
  }
}
