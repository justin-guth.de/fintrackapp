import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private emailInput;
  private passwordInput;

  constructor(private authService: AuthService) { }

  ngOnInit() {

    this.emailInput = document.getElementById('loginEmailInput');
    this.passwordInput = document.getElementById('loginPasswordInput');
  }

  onLoginClicked() {

    const email = this.emailInput.value;
    const password = this.passwordInput.value;

    this.authService.login(email, password);
  }

  onForgotPasswordClicked() {

  }

}
