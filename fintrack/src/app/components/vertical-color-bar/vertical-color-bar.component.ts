import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-vertical-color-bar',
  templateUrl: './vertical-color-bar.component.html',
  styleUrls: ['./vertical-color-bar.component.scss'],
})
export class VerticalColorBarComponent implements OnInit {

  @Input() color: string;

  constructor() { }

  ngOnInit() {}

}
