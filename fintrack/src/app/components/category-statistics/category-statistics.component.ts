import { EntryService } from 'src/app/services/entry.service';
import { CategoryService } from 'src/app/services/category.service';
import { CountedCategory } from './../../types/counted-category';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-category-statistics',
  templateUrl: './category-statistics.component.html',
  styleUrls: ['./category-statistics.component.scss'],
})
export class CategoryStatisticsComponent implements OnInit {

  public categories: CountedCategory[];

  constructor(private categoryService: CategoryService, private entryService: EntryService) { }

  ngOnInit() {

    this.refresh();
    this.entryService.addOnChangeCallback(() => {

      this.refresh();
    });

  }

  refresh() {

    this.categoryService.getCategoryStatistics((result: CountedCategory[]) => {

      this.categories = result;
    });
  }
}
