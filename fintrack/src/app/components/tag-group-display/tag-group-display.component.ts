import { EntryService } from './../../services/entry.service';
import { TagService } from './../../services/tag.service';
import { CountedTag } from './../../types/counted-tag';
import { Component, OnInit } from '@angular/core';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';
import { registerLocaleData } from '@angular/common';

@Component({
  selector: 'app-tag-group-display',
  templateUrl: './tag-group-display.component.html',
  styleUrls: ['./tag-group-display.component.scss'],
})
export class TagGroupDisplayComponent implements OnInit {

  public tags: CountedTag[];

  constructor(private tagService: TagService, private entryService: EntryService) {
    registerLocaleData(localeDe, 'de-DE', localeDeExtra);
  }

  ngOnInit() {

    this.refresh();
    this.entryService.addOnChangeCallback(() => { this.refresh(); });
  }
  refresh() {

    this.tagService.getTagStatistics((tags: CountedTag[]) => {
      this.tags = tags;
    });
  }

}
