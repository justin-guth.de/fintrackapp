import { Deficit } from './../../types/deficit';
import { EntryService } from 'src/app/services/entry.service';
import { DeficitService } from './../../services/deficit.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-deficit-display',
  templateUrl: './deficit-display.component.html',
  styleUrls: ['./deficit-display.component.scss'],
})
export class DeficitDisplayComponent implements OnInit {
  public deficit: Deficit;

  constructor(
    private deficitService: DeficitService,
    private entryService: EntryService
  ) {}

  ngOnInit() {
    this.entryService.addOnChangeCallback(() => {
      this.refresh();
    });
    this.refresh();
  }

  refresh() {
    this.deficitService.getDeficit((deficit: Deficit) => {
      this.deficit = deficit;
    });
  }

  updateRange(beginDate: string, endDate: string, showAll: boolean): void {
    if (showAll) {
      this.deficitService.getDeficit((deficit: Deficit) => {
        this.deficit = deficit;
      });
    } else {
      this.deficitService.getDeficitInRange(
        beginDate,
        endDate,
        (deficit: Deficit) => {
          this.deficit = deficit;
        }
      );
    }
  }
}
