import { SlimCategory } from './../../types/slim-category';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-category-box',
  templateUrl: './category-box.component.html',
  styleUrls: ['./category-box.component.scss'],
})
export class CategoryBoxComponent implements OnInit {



  @Input() category: SlimCategory;
  @Input() iconName: string = null;

  public customColor: string = null;
  public customIcon: string = null;

  constructor() { }

  ngOnInit() {

  }

  setIcon(icon: string) {

    this.customIcon = icon;
  }


  setColor(code: string) {

    this.customColor = code;
  }
}
