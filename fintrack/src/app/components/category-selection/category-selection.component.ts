import { Router } from '@angular/router';
import { CategoryTreeNode } from './../../types/category-tree-node';
import { CategoryService } from './../../services/category.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-category-selection',
  templateUrl: './category-selection.component.html',
  styleUrls: ['./category-selection.component.scss'],
})
export class CategorySelectionComponent implements OnInit {

  @Input() hideManageCategories: boolean;

  public selectionChain: CategoryTreeNode[] = [];
  public options: CategoryTreeNode[] = [];
  public defaultCategory: CategoryTreeNode;

  private incoming: CategoryTreeNode;
  private outgoing: CategoryTreeNode;
  private hidden: string[] = [];

  private ready = false;
  private setWhenReady: string;

  constructor(private categoryService: CategoryService, private router: Router) {

    this.hideManageCategories = false;
  }

  hide(id: string) {
    this.hidden.push(id);
  }

  ngOnInit() {

    this.categoryService.getCategoryTree((tree: { outgoing: CategoryTreeNode; incoming: CategoryTreeNode }) => {

      this.incoming = tree.incoming;
      this.outgoing = tree.outgoing;
      this.defaultCategory = tree.outgoing.clone();
      this.defaultCategory.color = '#919191';

      this.setOptions();
      this.ready = true;

      if (this.setWhenReady != null && this.setWhenReady !== undefined) {

        this.setSelected(this.setWhenReady);
      }
    });
  }

  setInitial(categoryId: string) {

    if (!this.ready) {

      this.setWhenReady = categoryId;
    }
    else {

      this.setSelected(categoryId);
    }
  }


  setSelected(categoryId: string) {

    if (this.incoming.containsId(categoryId)) {

      this.selectionChain = this.incoming.getChainTo(categoryId);
    } else if (this.outgoing.containsId(categoryId)) {

      this.selectionChain = this.outgoing.getChainTo(categoryId);
    } else {
      console.error('Could not locate id in tree:', categoryId, this.outgoing, this.incoming);
    }

    this.setOptions();
  }


  reset() {
    this.selectionChain = [];
    this.setOptions();
  }

  setOptions() {

    if (this.selectionChain.length === 0) {

      this.options = [this.incoming, this.outgoing];
    }
    else {

      this.options = this.selectionChain[this.selectionChain.length - 1].children.filter(
        (node) => !this.hidden.some(
          (elem) => elem === node.id
        )
      );
    }
  }

  select(node: CategoryTreeNode) {

    this.selectionChain.push(node);
    this.setOptions();
  }

  deselect(node: CategoryTreeNode) {

    const newChain: CategoryTreeNode[] = [];

    for (const elem of this.selectionChain) {

      if (elem.id !== node.id) {

        newChain.push(elem);
      }
      else {

        break;
      }
    }

    this.selectionChain = newChain;

    this.setOptions();
  }

  getSelection(): string {

    if (this.selectionChain.length === 0) {

      return this.defaultCategory.id;
    }
    else {
      return this.selectionChain[this.selectionChain.length - 1].id;
    }
  }

  navigateCategoryManagement() {

    this.router.navigate(['manage-categories']);
  }
}
