import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';
import { registerLocaleData } from '@angular/common';
import { CurrentPlatformService } from './../../services/current-platform-service.service';
import { StatisticsPage } from './../statistics/statistics.page';
import { EntryService } from 'src/app/services/entry.service';
import { Entry } from './../../types/entry';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { alertController } from '@ionic/core';
import { IonButton, Platform } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-entry-list-item',
  templateUrl: './entry-list-item.component.html',
  styleUrls: ['./entry-list-item.component.scss'],
})
export class EntryListItemComponent implements OnInit {

  @Input() entry: Entry;
  @Input() parent: any; // TODO fix with interface
  @Input() long: boolean; // TODO fix with interface

  @ViewChild('deleteButton') deleteButton: IonButton;
  @ViewChild('detailsButton') detailsButton: IonButton;

  public isMobile: boolean;

  constructor(
    private entryService: EntryService,
    private router: Router,
    private currentPlatformService: CurrentPlatformService,
    public platform: Platform) {

    registerLocaleData(localeDe, 'de-DE', localeDeExtra);
  }

  ngOnInit() {

    this.isMobile = this.currentPlatformService.isMobile() || this.currentPlatformService.isNative();
  }

  deleteEntry() {

    alertController.create({
      header: 'Are you sure you want to delete this entry?',
      message: 'This action is irreversible.', // TODO
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            // console.log('Cancelled');
          }
        },
        {
          text: 'Delete',
          role: 'delete',
          cssClass: 'secondary',
          handler: () => {
            console.log('Deleting entry:', this.entry.id);

            this.deleteButton.disabled = true;
            this.detailsButton.disabled = true;

            this.entryService.deleteEntry(this.entry.id, (response) => {

              this.notifyOnDelete();
            });
          }
        }
      ]
    }).then((alert) => {

      alert.present();
    });


  }

  notifyOnDelete() {

    this.parent?.refresh();
  }

  showDetails() {

    this.router.navigate(['details/' + this.entry.id]);
  }

}
