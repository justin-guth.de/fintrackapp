import { EntryService } from 'src/app/services/entry.service';
import { alertController } from '@ionic/core';
import { IonButton } from '@ionic/angular';
import { TagReplacerService } from './../../services/tag-replacer.service';
import { Entry } from 'src/app/types/entry';
import { Component, Input, OnInit, AfterViewInit, OnChanges, SecurityContext, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-entry-details',
  templateUrl: './entry-details.component.html',
  styleUrls: ['./entry-details.component.scss'],
})
export class EntryDetailsComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() entry: Entry;

  @ViewChild('deleteButton') deleteButton: IonButton;
  @ViewChild('editButton') editButton: IonButton;

  public descriptionText: string;

  constructor(
    private tagReplacerService: TagReplacerService,
    private sanitizer: DomSanitizer,
    private entryService: EntryService,
    private location: Location,
    private router: Router) { }

  ngOnInit() {

  }
  ngAfterViewInit() {
  }
  ngOnChanges() {
    if (this.entry != null && this.entry !== undefined) {

      this.descriptionText = this.tagReplacerService.replaceTags(this.entry.description, this.entry.tags);
    }
  }
  deleteEntry() {

    alertController.create({
      header: 'Are you sure you want to delete this entry?',
      message: 'This action is irreversible.', // TODO
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            // console.log('Cancelled');
          }
        },
        {
          text: 'Delete',
          role: 'delete',
          cssClass: 'secondary',
          handler: () => {
            // console.log('Deleting entry:', this.entry.id);

            this.deleteButton.disabled = true;
            this.editButton.disabled = true;

            this.entryService.deleteEntry(this.entry.id, (response) => {

              // Route back to list:
              this.location.back();
            });
          }
        }
      ]
    }).then((alert) => {

      alert.present();
    });
  };
  editEntry() {
    this.router.navigate(['edit/' + this.entry.id]);
  };

}
