import { CategoryTreeNode } from './../../types/category-tree-node';
import { CategorySelectionComponent } from './../category-selection/category-selection.component';
import { EntryService } from './../../services/entry.service';
import { CategoryService } from './../../services/category.service';
import { AmountInputComponent } from './../amount-input/amount-input.component';
import { Entry } from './../../types/entry';
import { Component, Input, OnInit, ViewChild, AfterViewInit, OnChanges } from '@angular/core';
import { IonDatetime, IonSelect, IonTextarea } from '@ionic/angular';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-entry',
  templateUrl: './edit-entry.component.html',
  styleUrls: ['./edit-entry.component.scss'],
})
export class EditEntryComponent implements OnInit, OnChanges {

  @Input() entry: Entry;

  @ViewChild('submitButton') submitButton: HTMLIonButtonElement;
  @ViewChild('titleInput') titleInput: HTMLIonInputElement;
  @ViewChild('amountInput') amountInput: AmountInputComponent;
  @ViewChild('categoryInput') categoryInput: CategorySelectionComponent;
  @ViewChild('descriptionInput') descriptionInput: IonTextarea;
  @ViewChild('dateInput') dateInput: IonDatetime;

  private isSubmitting: boolean = false;



  constructor(
    private categoryService: CategoryService,
    private entryService: EntryService,
    private location: Location,
  ) { }

  ngOnInit() {

  }

  ngOnChanges() {

    if (this.entry != null && this.entry !== undefined) {

      this.categoryInput.setInitial(this.entry.category_id);
    }
  }
  checkButton() {

    if (this.isSubmitting) {

      this.submitButton.disabled = true;
      return;
    }

    const title = this.titleInput.value;
    const amount = this.amountInput?.getValue();

    if (title === '' || title === null || title === undefined || amount === 0) {

      this.submitButton.disabled = true;
    }
    else {

      this.submitButton.disabled = false;
    }
  }

  onTitleChange() {

    this.checkButton();
  }

  submit() {

    this.entryService.updateEntry(
      this.entry.id,
      this.titleInput.value as string,
      this.amountInput.getValue(),
      this.categoryInput.getSelection(),
      this.descriptionInput.value,
      this.dateInput.value,
      (response) => {

        this.location.back();

      });
  }

}
