import { Component, Input, OnInit, AfterViewInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-amount-input',
  templateUrl: './amount-input.component.html',
  styleUrls: ['./amount-input.component.scss'],
})
export class AmountInputComponent implements OnInit, OnChanges {

  @Input() value: number;

  public currencyDisplayValue = '0,00';

  constructor() { }

  getValue(): number {
    return + this.currencyDisplayValue.replace(/\D/g, '');
  }

  ngOnInit() {


  }
  ngOnChanges() {

    this.updateInputField(this.value);
  }



  onInput() {

    setTimeout(() => {
      let input = this.currencyDisplayValue;
      input = input.replace(/\D/g, '');
      const amount = + input;

      this.updateInputField(amount);
    }, 5);
  }

  updateInputField(amount) {

    this.value = amount;

    if (amount === undefined || amount === null || amount < 0) {
      amount = 0;
    }

    let zeroRepeat = 3 - amount.toString().length;
    if (zeroRepeat < 0) {
      zeroRepeat = 0;
    }

    const amountString = '0'.repeat(zeroRepeat) + amount;


    this.currencyDisplayValue = amountString.substring(0, amountString.length - 2)
      + ',' + amountString.substring(amountString.length - 2, amountString.length);
  }
}
