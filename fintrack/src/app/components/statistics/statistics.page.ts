import { SearchService } from './../../services/search.service';
import { HiddenListingAreaComponent } from './../hidden-listing-area/hidden-listing-area.component';
import { DateRange } from './../../types/date-range';
import { DeficitDisplayComponent } from './../deficit-display/deficit-display.component';
import { DateService } from './../../services/date.service';
import { IonDatetime } from '@ionic/angular';
import { AfterViewInit } from '@angular/core';
/* eslint-disable @typescript-eslint/quotes */
import { Location } from '@angular/common';
import {
  AfterViewChecked,
  COMPILER_OPTIONS,
  Component,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';
import { EntryService } from 'src/app/services/entry.service';
import { CategoryTreeNode } from 'src/app/types/category-tree-node';
import { Entry } from 'src/app/types/entry';
import { Sankey } from 'src/app/types/sankey';
import { SankeyNode } from 'src/app/types/sankey-node';
import { EntryListItemComponent } from '../entry-list-item/entry-list-item.component';
import { SearchbarChangeEventDetail } from '@ionic/core';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.page.html',
  styleUrls: ['./statistics.page.scss'],
})
export class StatisticsPage implements OnInit, AfterViewInit {
  @ViewChild('beginDateInput') beginDateInput: IonDatetime;
  @ViewChild('endDateInput') endDateInput: IonDatetime;
  @ViewChild('deficitDisplay') deficitDisplay: DeficitDisplayComponent;
  @ViewChild('hiddenListingArea') hiddenListingArea: HiddenListingAreaComponent;

  canvas: HTMLCanvasElement;
  s: Sankey;
  loadingList: boolean;
  entryList: Entry[];
  searchEntryList: Entry[];
  fullEntryList: Entry[];
  public showAllTime: boolean = true;

  private incomingCategories: CategoryTreeNode;
  private outgoingCategories: CategoryTreeNode;
  private searchPattern: string = "";

  // @ViewChildren(EntryListItemComponent) entryListItems: QueryList<EntryListItemComponent>;

  constructor(
    private entryService: EntryService,
    private categoryService: CategoryService,
    private dateService: DateService,
    private searchService: SearchService
  ) { }

  ngAfterViewInit() {
    this.canvas = document.getElementById('sankeyCanvas') as HTMLCanvasElement;

    this.scaleCanvasToParent();

    window.addEventListener('resize', (ev) => {
      this.scaleCanvasToParent();
      this.s?.recomputePixelPerAmount();
      this.s?.paint();
    });

    this.refreshList();
  }

  ngOnInit() {
    this.scaleCanvasToParent();

    this.entryService.addOnChangeCallback(() => {
      this.refreshList();
    });
  }

  showLoadingList() {
    this.loadingList = true;
  }

  hideLoadingList() {
    this.loadingList = false;
  }

  scaleCanvasToParent() {
    if (this.canvas == null) {
      return;
    }

    this.canvas.width = (this.canvas.parentNode as HTMLDivElement).clientWidth;
    this.canvas.height = (
      this.canvas.parentNode as HTMLDivElement
    ).clientHeight;
  }

  refresh() {
    this.refreshList();
  }

  resetSankey() {

    this.s = new Sankey(this.canvas, this.hiddenListingArea);

    const incomingSankeyNodes: SankeyNode = SankeyNode.fromCategoryTreeNode(
      this.incomingCategories,
      this.s,
      this.hiddenListingArea,
    );

    const outgoingSankeyNodes: SankeyNode = SankeyNode.fromCategoryTreeNode(
      this.outgoingCategories,
      this.s,
      this.hiddenListingArea,

    );

    this.s.init(
      incomingSankeyNodes,
      outgoingSankeyNodes
    );


    for (const element of this.entryList) {
      this.s.tryToAdd(element.category_id, element.amount);
    }

    this.scaleCanvasToParent();
    this.s?.recomputePixelPerAmount();
    this.s?.paint();

    // the width of the parent div is wrong initially...
    // This must be due to the later loading of the entry list below which leads to the appearance of a scrollbar
    setTimeout(() => {
      this.scaleCanvasToParent();
      this.s?.recomputePixelPerAmount();
      this.s?.paint();
    }, 1);
  }

  refreshList() {
    this.showLoadingList();

    this.categoryService.getCategoryTree(
      (result: { outgoing: CategoryTreeNode; incoming: CategoryTreeNode }) => {
        this.entryService.getEntries((list: Entry[]) => {
          this.hideLoadingList();
          this.entryList = list;
          this.fullEntryList = list;
          this.searchEntryList = this.entryList.filter((entry: Entry) =>
            this.searchService.matches(entry, this.searchPattern)
          );

          this.incomingCategories = result.incoming;
          this.outgoingCategories = result.outgoing;

          this.resetSankey();
          this.updateDateRange(
            this.beginDateInput.value,
            this.endDateInput.value
          );
        });
      }
    );
  }

  dateSelectionChanged(): void {
    const beginDate = this.beginDateInput.value;
    const endDate = this.endDateInput.value;
    this.showAllTime = false;

    if (this.dateService.isInOrder(beginDate, endDate)) {
      this.updateDateRange(beginDate, endDate);
    }
  }

  updateDateRange(beginDate: string, endDate: string): void {
    beginDate = this.dateService.normaliseDate(beginDate);
    endDate = this.dateService.normaliseDate(endDate);

    if (this.fullEntryList === undefined || this.fullEntryList === null) {
      return;
    }

    if (this.showAllTime) {
      this.entryList = this.fullEntryList;
      this.searchEntryList = this.entryList.filter((entry: Entry) =>
        this.searchService.matches(entry, this.searchPattern)
      );
    } else {
      this.entryList = this.fullEntryList.filter((entry: Entry) =>
        this.dateService.isInBounds(beginDate, entry.date, endDate)
      );
      this.searchEntryList = this.entryList.filter((entry: Entry) =>
        this.searchService.matches(entry, this.searchPattern)
      );

    }

    this.deficitDisplay.updateRange(beginDate, endDate, this.showAllTime);
    this.resetSankey();
  }

  setDateRange(range: DateRange) {

    this.beginDateInput.value = this.dateService.normaliseDate(
      range.from.toISOString()
    );
    this.endDateInput.value = this.dateService.normaliseDate(
      range.to.toISOString()
    );


    this.updateDateRange(this.beginDateInput.value, this.endDateInput.value);
  }

  thisMonth() {
    this.showAllTime = false;
    const today = new Date();
    const range: DateRange = this.dateService.getMonthRange(today);
    this.setDateRange(range);
  }

  lastMonth() {
    this.showAllTime = false;
    const today = new Date();
    const range: DateRange = this.dateService.getLastMonthRange(today);
    this.setDateRange(range);
  }

  thisYear() {
    this.showAllTime = false;
    const today = new Date();
    const range: DateRange = this.dateService.getYearRange(today);
    this.setDateRange(range);
  }

  lastYear() {
    this.showAllTime = false;
    const today = new Date();
    const range: DateRange = this.dateService.getLastYearRange(today);
    this.setDateRange(range);
  }

  allTime() {
    this.showAllTime = true;
    this.resetSankey();
    this.updateDateRange(this.beginDateInput.value, this.endDateInput.value);
  }

  search(event: any) {

    this.searchPattern = event.target.value;

    this.searchEntryList = this.entryList.filter((entry: Entry) =>
      this.searchService.matches(entry, this.searchPattern)
    );

  }
}
