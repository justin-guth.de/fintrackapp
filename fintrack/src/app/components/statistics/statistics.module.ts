import { HiddenListingAreaModule } from './../../modules/hidden-listing-area/hidden-listing-area.module';
import { DeficitDisplayModule } from './../../modules/deficit-display/deficit-display.module';
import { CollapsibleCardModule } from '../../modules/collapsible-card/collapsible-card.module';
import { CategoryStatisticsModule } from './../../modules/category-statistics/category-statistics.module';
import { TagGroupDisplayModule } from './../../modules/tag-group-display/tag-group-display.module';
import { EntryListItemModuleModule } from './../../entry-list-item-module/entry-list-item-module.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StatisticsPageRoutingModule } from './statistics-routing.module';

import { StatisticsPage } from './statistics.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StatisticsPageRoutingModule,
    EntryListItemModuleModule,
    TagGroupDisplayModule,
    CategoryStatisticsModule,
    CollapsibleCardModule,
    DeficitDisplayModule,
    HiddenListingAreaModule
  ],
  declarations: [StatisticsPage]
})
export class StatisticsPageModule { }
