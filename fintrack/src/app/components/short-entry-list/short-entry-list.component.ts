import { ApiResponse } from './../../types/api-response';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EntryService } from 'src/app/services/entry.service';
import { Entry } from 'src/app/types/entry';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-short-entry-list',
  templateUrl: './short-entry-list.component.html',
  styleUrls: ['./short-entry-list.component.scss'],
})
export class ShortEntryListComponent implements OnInit {

  spinner: HTMLIonSpinnerElement;

  public entries: Entry[];

  constructor(private entryService: EntryService, private router: Router) { }


  ngOnInit() {

    this.spinner = document.getElementById('entryListSpinner') as HTMLIonSpinnerElement;
    this.reloadEntries();
    this.entryService.addOnChangeCallback(()=> {

      this.reloadEntries();
    });
  }

  deleteEntry(id: string) {

    (document.getElementById('deleteButton_' + id) as HTMLIonButtonElement).disabled = true;
    this.entryService.deleteEntry(id, (response) => {

      this.reloadEntries();
    });
  }

  reloadEntries() {

    this.showLoadingSpinner();

    this.entryService.getLastNEntries(7, (result: Entry[]) => {

        this.hideLoadingSpinner();
        this.entries = result;
    });
  }


  hideLoadingSpinner() {
    this.spinner.style.display = 'none';
  }

  showLoadingSpinner() {
    this.spinner.style.display = 'block';
  }

  routeToStatistics() {

    this.router.navigate(['statistics']);
  }

  loadEntryDetails(id) {


  }

}
