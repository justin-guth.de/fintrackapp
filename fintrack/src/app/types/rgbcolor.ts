import { HSVColor } from './hsvcolor';
export class RGBColor {


  constructor(
    public r: number = 0,
    public g: number = 0,
    public b: number = 0
  ) { }

  static fromCode(color: string): RGBColor {

    return new RGBColor(
      parseInt(color.substr(1, 2), 16),
      parseInt(color.substr(3, 2), 16),
      parseInt(color.substr(5, 2), 16)
    );
  }


  code(): string {

    const hexR: string = (this.r < 16 ? '0' : '') + this.r.toString(16);
    const hexG: string = (this.g < 16 ? '0' : '') + this.g.toString(16);
    const hexB: string = (this.b < 16 ? '0' : '') + this.b.toString(16);

    return '#' + hexR + hexG + hexB;
  }

  clone(): HSVColor {
    return new HSVColor(this.r, this.g, this.b);
  }

  public toHSV(): HSVColor {

    const rPrime = this.r / 255;
    const gPrime = this.g / 255;
    const bPrime = this.b / 255;

    const cMax = Math.max(rPrime, gPrime, bPrime);
    const cMin = Math.min(rPrime, gPrime, bPrime);

    const delta = cMax - cMin;

    let h: number = 0;

    if (delta === 0) {

      h = 0;
    }
    else if (cMax === rPrime) {
      h = 60 * (((gPrime - bPrime) / delta) % 6);
    }
    else if (cMax === gPrime) {
      h = 60 * ((((bPrime - rPrime) / delta) + 2) % 6);
    }
    else if (cMax === bPrime) {
      h = 60 * ((((rPrime - gPrime) / delta) + 4) % 6);
    }

    let s: number = 0;

    if (cMax === 0) {
      s = 0;
    } else {
      s = delta / cMax;
    }

    const v = cMax;

    return new HSVColor(h, s, v);
  }
}
