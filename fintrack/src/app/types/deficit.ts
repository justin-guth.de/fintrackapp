export interface Deficit {

  income: number;
  expenses: number;
  deficit: number;
}
