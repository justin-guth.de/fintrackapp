export class CategoryTreeNode {

  public name: string;
  public id: string;
  public children: CategoryTreeNode[] = [];
  public color: string;
  public icon: string;

  constructor(name: string, id: string, color: string, icon: string) {
    this.name = name;
    this.color = color;
    this.id = id;
    this.icon = icon;
  }

  public static fromArrayTree(tree): CategoryTreeNode {

    const result: CategoryTreeNode = new CategoryTreeNode(
      tree.category.name,
      tree.category.id,
      tree.category.color ?? '#22CCDD', tree.category.icon
    );

    for (let i = 0; i < (tree.children as any[]).length; i++) {

      result.children.push(CategoryTreeNode.fromArrayTree(tree.children[i]));
    }

    return result;
  }

  clone(): CategoryTreeNode {
    return new CategoryTreeNode(this.name, this.id, this.color, this.icon);
  }

  getChainTo(id: string): CategoryTreeNode[] {

    if (this.id === id) {
      return [this];
    }

    for (const child of this.children) {

      if (child.containsId(id)) {

        return ([this] as CategoryTreeNode[]).concat(child.getChainTo(id));
      }
    }

    return [];
  }

  public containsId(id: string) {

    if (this.id === id) {
      return true;
    }

    for (const child of this.children) {

      if (child.containsId(id)) {

        return true;
      }
    }

    return false;
  }
}
