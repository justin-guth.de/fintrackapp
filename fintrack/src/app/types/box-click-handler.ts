import { SankeyNode } from './sankey-node';
export class BoxClickHandler {


  private static listeners: ((event: MouseEvent) => void)[] = [];
  private x: number;
  private y: number;

  private w: number;
  private h: number;

  private node: SankeyNode;


  private eventHandler: (event: MouseEvent) => void;
  private canvas: HTMLCanvasElement;

  constructor(
    x: number,
    y: number,
    w: number,
    h: number,
    node: SankeyNode,
    hiddenListingArea: any, // todo,
    canvas: HTMLCanvasElement
  ) {

    this.set(x, y, w, h);
    this.node = node;
    this.canvas = canvas;

    this.eventHandler = (event: MouseEvent) => {

      const bRect: DOMRect = this.canvas.getBoundingClientRect();

      const newCursorPositionX = (event.clientX - bRect.left);
      const newCursorPositionY = (event.clientY - bRect.top);

      if (newCursorPositionX < this.x || newCursorPositionX > (this.x + this.w)
        || newCursorPositionY < this.y || newCursorPositionY > (this.y + this.h)) {
        return;
      }

      this.node.hide();
      window.removeEventListener('click', BoxClickHandler.listeners[this.node.id]);
      BoxClickHandler.listeners[this.node.id] = null;
      console.log(this.node);

    };

    if (BoxClickHandler.listeners[this.node.id] !== undefined && BoxClickHandler.listeners[this.node.id] !== null) {
      window.removeEventListener('click', BoxClickHandler.listeners[this.node.id]);
    }

    window.addEventListener('click', this.eventHandler);
    BoxClickHandler.listeners[this.node.id] = this.eventHandler;
  }

  public set(
    x: number,
    y: number,
    w: number,
    h: number
  ) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

}
