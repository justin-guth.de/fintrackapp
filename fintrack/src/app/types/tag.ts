export interface Tag {

  id: string;
  color: string;
  tag: string;
}
