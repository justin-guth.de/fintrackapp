import { TextPaintInstruction } from './text-paint-instruction';

export interface SankeyNodeBoxPositionInfo {

  x: number;
  y: number;
  height: number;
  barHeight: number;
  width: number;
  text: string;
  color: string;
  textPaintInstructions: TextPaintInstruction[];
  rootY: number;
}
