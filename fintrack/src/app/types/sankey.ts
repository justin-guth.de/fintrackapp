import { HiddenListingAreaComponent } from './../components/hidden-listing-area/hidden-listing-area.component';
import { SankeyNode } from './sankey-node';
import { SankeyNodeBoxPositionInfo } from './sankey-node-box-position-info';
import { SankeySettings } from './sankey-settings';
import { TextPaintInstruction } from './text-paint-instruction';

export class Sankey {

  private defaultSettings: SankeySettings = {
    verticalBarPadding: 15,
    verticalBarWidth: 10,

    verticalBorderPadding: 15,
    horizontalBorderPadding: 15,

    textMargin: 8
  };


  private canvas: HTMLCanvasElement;

  private inboundNode: SankeyNode;
  private outboundNode: SankeyNode;
  private settings: SankeySettings;
  private pixelPerAmount: number;
  private alphaSuffix = '30';
  private hiddenListingArea: HiddenListingAreaComponent;

  constructor(
    canvas: HTMLCanvasElement,
    hiddenListingArea: HiddenListingAreaComponent,
    settings?: SankeySettings
  ) {

    this.canvas = canvas;
    this.hiddenListingArea = hiddenListingArea;
    this.settings = settings ?? this.defaultSettings;

  }

  public init(
    inboundRootNode: SankeyNode,
    outboundRootNode: SankeyNode,
  ) {
    this.inboundNode = inboundRootNode;
    this.outboundNode = outboundRootNode;
  }

  public paint() {

    const ctx = this.canvas.getContext('2d');
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    const paintInfo1: SankeyNodeBoxPositionInfo = this.inboundNode.paintOnCanvas(
      this.canvas,
      this.canvas.width / 2 - (this.settings.verticalBarWidth ?? this.defaultSettings.verticalBarWidth) / 2,
      0,// + this.settings.horizontalBorderPadding ?? this.defaultSettings.horizontalBorderPadding,
      this.settings.verticalBorderPadding ?? this.defaultSettings.verticalBorderPadding,
      this.canvas.height - this.settings.verticalBorderPadding ?? this.defaultSettings.verticalBorderPadding,
      this.pixelPerAmount,
      this.settings.verticalBarWidth ?? this.defaultSettings.verticalBarWidth,
      this.settings.verticalBarPadding ?? this.defaultSettings.verticalBarPadding,
      this.settings.textMargin ?? this.defaultSettings.textMargin,
      true,
      this.alphaSuffix,
      false
    );

    const paintInfo2: SankeyNodeBoxPositionInfo = this.outboundNode.paintOnCanvas(
      this.canvas,
      this.canvas.width / 2 + (this.settings.verticalBarWidth ?? this.defaultSettings.verticalBarWidth) / 2,
      this.canvas.width - this.settings.horizontalBorderPadding ?? this.defaultSettings.horizontalBorderPadding,
      this.settings.verticalBorderPadding ?? this.defaultSettings.verticalBorderPadding,
      this.canvas.height - this.settings.verticalBorderPadding ?? this.defaultSettings.verticalBorderPadding,
      this.pixelPerAmount,
      this.settings.verticalBarWidth ?? this.defaultSettings.verticalBarWidth,
      this.settings.verticalBarPadding ?? this.defaultSettings.verticalBarPadding,
      this.settings.textMargin ?? this.defaultSettings.textMargin,
      false,
      this.alphaSuffix,
      false
    );

    const context: CanvasRenderingContext2D = this.canvas.getContext('2d');

    let paintInstructions: TextPaintInstruction[] = [];

    if (paintInfo1 != null) {

      paintInstructions = paintInstructions.concat(paintInfo1.textPaintInstructions);
    }

    if (paintInfo2 != null) {

      paintInstructions = paintInstructions.concat(paintInfo2.textPaintInstructions);
    }

    let x;
    let y;
    let height;


    if (
      paintInfo1 != null && paintInfo2 == null
      || paintInfo1 != null && paintInfo2 != null && paintInfo1.barHeight > paintInfo2.barHeight
    ) {

      y = this.canvas.height / 2 - paintInfo1.barHeight / 2;
      x = this.canvas.width / 2 - (this.settings.verticalBarWidth ?? this.defaultSettings.verticalBarWidth) / 2;

      height = paintInfo1.barHeight;

    } else if (paintInfo2 != null) {

      y = this.canvas.height / 2 - paintInfo2.barHeight / 2;
      x = this.canvas.width / 2 - (this.settings.verticalBarWidth ?? this.defaultSettings.verticalBarWidth) / 2;

      height = paintInfo2.barHeight;

    }
    else {
      y = 0;
      x = this.canvas.width / 2 - (this.settings.verticalBarWidth ?? this.defaultSettings.verticalBarWidth) / 2;
      height = this.canvas.height;
    }

    context.fillStyle = '#FFFFFF';
    context.fillRect(x, y, this.settings.verticalBarWidth ?? this.defaultSettings.verticalBarWidth, height);

    if (paintInfo1 != null) {
      // paint bezier to input node

      const xIn = paintInfo1.x + this.settings.verticalBarWidth ?? this.defaultSettings.verticalBarWidth;
      const yIn = paintInfo1.y;
      const xmid = (x + xIn) / 2;

      context.beginPath();
      context.moveTo(x, y);
      context.bezierCurveTo(xmid, y, xmid, yIn, xIn, yIn);
      context.lineTo(xIn, yIn + paintInfo1.barHeight);
      context.bezierCurveTo(xmid, yIn + paintInfo1.barHeight, xmid, y + paintInfo1.barHeight, x, y + paintInfo1.barHeight);
      context.closePath();

      context.fillStyle = paintInfo1.color + this.alphaSuffix;
      context.fill();
    }

    if (paintInfo2 != null) {

      // paint bezier to output node

      const xOut = paintInfo2.x;
      const yOut = paintInfo2.y;
      const xCenter = x + this.settings.verticalBarWidth ?? this.defaultSettings.verticalBarWidth;
      const xmid = (xCenter + xOut) / 2;

      context.beginPath();
      context.moveTo(xCenter, y);
      context.bezierCurveTo(xmid, y, xmid, yOut, xOut, yOut);
      context.lineTo(xOut, yOut + paintInfo2.barHeight);
      context.bezierCurveTo(xmid, yOut + paintInfo2.barHeight, xmid, y + paintInfo2.barHeight, xCenter, y + paintInfo2.barHeight);
      context.closePath();

      context.fillStyle = paintInfo2.color + this.alphaSuffix;
      context.fill();
    }

    // paint last text elements
    if (paintInfo1 != null) {

      context.fillStyle = '#FFFFFF';
      context.font = '16px sans-serif';

      const textMeasurement: TextMetrics = context.measureText(paintInfo1.text);
      const textHeight = (textMeasurement.actualBoundingBoxAscent + textMeasurement.actualBoundingBoxDescent);


      paintInstructions.push(
        {
          text: paintInfo1.text,
          x: (paintInfo1.x
            + (this.settings.verticalBarWidth ?? this.defaultSettings.verticalBarWidth)
            + (this.settings.textMargin ?? this.defaultSettings.textMargin)),
          y: paintInfo1.rootY + (paintInfo1.height / 2) + (textHeight / 2)
        }
      );
    }

    if (paintInfo2 != null) {

      const textMeasurement = context.measureText(paintInfo2.text);
      const textHeight = (textMeasurement.actualBoundingBoxAscent + textMeasurement.actualBoundingBoxDescent);
      const textWidth = textMeasurement.width;

      paintInstructions.push(
        {
          text: paintInfo2.text,
          x: (paintInfo2.x
            - (this.settings.verticalBarWidth ?? this.defaultSettings.verticalBarWidth)
            - (this.settings.textMargin ?? this.defaultSettings.textMargin)
            - textWidth),
          y: paintInfo2.y + (paintInfo2.barHeight / 2) + (textHeight / 2)
        }
      );
    }

    for (const instruction of paintInstructions) {

      context.fillText(instruction.text, instruction.x, instruction.y);
    }


  }

  public tryToAdd(id: string, value: number): boolean {

    value = Math.abs(value);

    if (!this.inboundNode.tryToAdd(id, value)) {
      return this.outboundNode.tryToAdd(id, value);
    }
    return true;
  }

  public recomputePixelPerAmount() {

    const outboundLeafCount = this.outboundNode.countLeafNodes(false);
    const inboundLeafCount = this.inboundNode.countLeafNodes(false);


    const outboundLeafAmount = this.outboundNode.countLeafValue(false);
    const inboundLeafAmount = this.inboundNode.countLeafValue(false);

    const outboundLeafPadding = (outboundLeafCount - 1) * (this.settings.verticalBarPadding ?? this.defaultSettings.verticalBarPadding);
    const inboundLeafPadding = (inboundLeafCount - 1) * (this.settings.verticalBarPadding ?? this.defaultSettings.verticalBarPadding);

    const leftoverOutboundSpace = this.canvas.height
      - 2 * (this.settings.verticalBorderPadding ?? this.defaultSettings.verticalBorderPadding)
      - outboundLeafPadding;

    const leftoverInboundSpace = this.canvas.height
      - 2 * (this.settings.verticalBorderPadding ?? this.defaultSettings.verticalBorderPadding)
      - inboundLeafPadding;


    const outboundPixelPerAmount = leftoverOutboundSpace / outboundLeafAmount;
    const inboundPixelPerAmount = leftoverInboundSpace / inboundLeafAmount;

    if (outboundPixelPerAmount > inboundPixelPerAmount) {
      this.pixelPerAmount = inboundPixelPerAmount;
    }
    else {
      this.pixelPerAmount = outboundPixelPerAmount;
    }

  }


}
