/* eslint-disable @typescript-eslint/naming-convention */
export interface Category {

  id: string;
  name: string;
  color: string;
  icon: string;
  parent_id: string;
  is_incoming: boolean;
  _meta?: any;
}
