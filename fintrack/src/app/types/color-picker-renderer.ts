import { HSVColor } from './hsvcolor';
import { RGBColor } from './rgbcolor';
export class ColorPickerRenderer {


  private canvas: HTMLCanvasElement;
  private context: CanvasRenderingContext2D;

  private isMouseDown: boolean = false;


  private color: HSVColor = new HSVColor(0, 0, 1);
  private cursorPositionX = 0.5;
  private cursorPositionY = 0.5;
  private cursorRadius: number = 8;
  private cursorInnerRadius: number = 6;
  private value: number = 1;
  private onChangeListeners: ((color: HSVColor) => void)[] = [];

  setColor(color: string) {

    this.color = (RGBColor.fromCode(color).toHSV());
    const deltaX = Math.cos((this.color.h / 360) * 2 * Math.PI);
    const deltaY = Math.sin((this.color.h / 360) * 2 * Math.PI);

    this.cursorPositionX = 0.5 + deltaX * this.color.s * 0.5;
    this.cursorPositionY = 0.5 + deltaY * this.color.s * 0.5;
    this.paint();
    this.notify();
  }

  init(canvas: HTMLCanvasElement) {

    this.canvas = canvas;
    this.context = canvas.getContext('2d');

    window.addEventListener('mousedown', (e) => this.mouseDown(e));
    window.addEventListener('mouseup', (e) => this.mouseUp(e));
    window.addEventListener('mousemove', (e) => this.mouseMove(e));
    this.paint();
  }

  addOnChangeListener(callback: (color: HSVColor) => void) {
    this.onChangeListeners.push(callback);
  }

  mouseMove(e: MouseEvent): void {

    if (!this.isMouseDown) {
      return;
    }


    this.positionMouse(e);
    this.paint();
  }

  mouseUp(e: MouseEvent): void {
    this.isMouseDown = false;
    this.positionMouse(e);
    this.paint();
  }

  positionMouse(e: MouseEvent) {

    const bRect: DOMRect = this.canvas.getBoundingClientRect();

    const newCursorPositionX = (e.clientX - bRect.left);
    const newCursorPositionY = (e.clientY - bRect.top);

    const centerX: number = this.canvas.width / 2;
    const centerY: number = this.canvas.height / 2;
    const radius: number = Math.min(centerX, centerY);

    const distanceXSquared = (newCursorPositionX - centerX) * (newCursorPositionX - centerX);
    const distanceYSquared = (newCursorPositionY - centerY) * (newCursorPositionY - centerY);

    if (distanceXSquared + distanceYSquared > radius * radius) {

      return;
    }

    this.cursorPositionX = newCursorPositionX / this.canvas.width;
    this.cursorPositionY = newCursorPositionY / this.canvas.height;

    this.computeColor();
    this.notify();
  }

  computeColor() {

    const centerX: number = this.canvas.width / 2;
    const centerY: number = this.canvas.height / 2;
    const radius: number = Math.min(centerX, centerY);

    const newCursorPositionX = (this.cursorPositionX * this.canvas.width);
    const newCursorPositionY = (this.cursorPositionY * this.canvas.height);

    const distanceXSquared = (newCursorPositionX - centerX) * (newCursorPositionX - centerX);
    const distanceYSquared = (newCursorPositionY - centerY) * (newCursorPositionY - centerY);

    const dot = (this.canvas.width - centerX) * (newCursorPositionX - centerX) + 0 * (newCursorPositionY - centerY);
    const det = (this.canvas.width - centerX) * (newCursorPositionY - centerY) - 0 * (newCursorPositionX - centerX);
    let angle = Math.atan2(det, dot) * 360 / (2 * Math.PI);

    if (newCursorPositionY < centerY) {

      angle = ((2 * Math.PI) + Math.atan2(det, dot)) * 360 / (2 * Math.PI);
    }

    this.color = new HSVColor(angle, Math.sqrt(distanceXSquared + distanceYSquared) / radius, this.value);
  }

  setValue(value: number) {

    if (value > 1) {
      this.value = 1;
    }
    else if (value < 0) {
      this.value = 0;
    }
    else {
      this.value = value;
    }

    this.computeColor();
    this.paint();
    this.notify();

  }

  mouseDown(e: MouseEvent): void {

    const bRect: DOMRect = this.canvas.getBoundingClientRect();

    const newCursorPositionX = (e.clientX - bRect.left);
    const newCursorPositionY = (e.clientY - bRect.top);

    const centerX: number = this.canvas.width / 2;
    const centerY: number = this.canvas.height / 2;
    const radius: number = Math.min(centerX, centerY);

    const distanceXSquared = (newCursorPositionX - centerX) * (newCursorPositionX - centerX);
    const distanceYSquared = (newCursorPositionY - centerY) * (newCursorPositionY - centerY);

    if (distanceXSquared + distanceYSquared > radius * radius) {

      return;
    }

    this.isMouseDown = true;
    this.positionMouse(e);
    this.paint();
  }

  paint() {

    this.clearCanvas();

    const centerX: number = this.canvas.width / 2;
    const centerY: number = this.canvas.height / 2;
    const radius: number = Math.min(centerX, centerY);


    const step = 2;
    const epsilon = 1;

    for (let deg = 0; deg < 360; deg += step) {

      const radAngle = (deg / 360) * 2 * Math.PI;

      const nextRadAngle = (((deg + step + epsilon) % 360) / 360) * 2 * Math.PI;
      const midRadAngle = (((deg + (step / 2)) % 360) / 360) * 2 * Math.PI;

      const sin = Math.sin(radAngle);
      const sinNext = Math.sin(nextRadAngle);
      const sinMid = Math.sin(midRadAngle);

      const cos = Math.cos(radAngle);
      const cosNext = Math.cos(nextRadAngle);
      const cosMid = Math.cos(midRadAngle);

      const destX = centerX + cosMid * radius;
      const destY = centerY + sinMid * radius;

      const sliceBeginX = centerX + cos * radius;
      const sliceBeginY = centerY + sin * radius;


      const gradient: CanvasGradient = this.context.createLinearGradient(centerX, centerY, destX, destY);

      gradient.addColorStop(0, (new HSVColor(deg, 0, this.value)).toRGB().code());

      gradient.addColorStop(1, (new HSVColor(deg, 1, this.value)).toRGB().code());

      this.context.fillStyle = gradient;
      this.context.beginPath();
      this.context.moveTo(centerX, centerY);
      this.context.lineTo(sliceBeginX, sliceBeginY);
      this.context.arc(centerX, centerY, radius, radAngle, nextRadAngle, false);
      this.context.closePath();
      this.context.fill();

    }

    // paint cursor
    this.context.beginPath();
    this.context.arc(
      this.canvas.width * this.cursorPositionX,
      this.canvas.height * this.cursorPositionY,
      this.cursorRadius,
      0,
      2 * Math.PI
    );

    this.context.fillStyle = '#000000';
    this.context.fill();

    this.context.beginPath();
    this.context.arc(
      this.canvas.width * this.cursorPositionX,
      this.canvas.height * this.cursorPositionY,
      this.cursorInnerRadius,
      0,
      2 * Math.PI
    );
    this.context.fillStyle = '#FFFFFF';
    this.context.fill();

  }

  getRGB(): RGBColor {

    return this.color.toRGB();
  }

  getHSV(): HSVColor {

    return this.color.clone();
  }

  clearCanvas() {

    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.fillStyle = (new HSVColor(0, 0, this.value)).toRGB().code();
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
  }

  private notify() {

    this.onChangeListeners.forEach((listener) => {
      listener(this.color);
    });
  }
}
