export interface TextPaintInstruction {

  text: string;
  x: number;
  y: number;
}
