/* eslint-disable @typescript-eslint/naming-convention */
export interface CreatedCategory {
  parent_id: string;
  name: string;
  color: string;
  id?: string;
  identifier?: string;
  parent_identifier?: string;
  account_id?: string;
  incoming?: boolean;
}
