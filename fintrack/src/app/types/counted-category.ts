export interface CountedCategory {

  id: string;
  name: string;
  identifier: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  parent_identifier: string;
  color: string;
  amount: number;
}
