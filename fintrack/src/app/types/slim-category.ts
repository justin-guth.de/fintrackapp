export interface SlimCategory {
  name: string;
  color: string;
  icon: string;
}
