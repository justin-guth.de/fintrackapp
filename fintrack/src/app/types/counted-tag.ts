export interface CountedTag {
  id: string;
  color: string;
  tag: string;
  amount: number;
}
