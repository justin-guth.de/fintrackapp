import { RGBColor } from './rgbcolor';
export class HSVColor {

  constructor(
    public h: number = 0,
    public s: number = 0,
    public v: number = 0
  ) { }

  clone(): HSVColor {
    return new HSVColor(this.h, this.s, this.v);
  }

  public toRGB(): RGBColor {

    const c = this.v * this.s;
    const hPrime = this.h / 60;

    const x = c * (1 - Math.abs((hPrime % 2) - 1));

    let rOne: number = 0;
    let gOne: number = 0;
    let bOne: number = 0;

    if (0 <= hPrime && hPrime < 1) {

      rOne = c;
      gOne = x;
      bOne = 0;
    }
    else if (1 <= hPrime && hPrime < 2) {

      rOne = x;
      gOne = c;
      bOne = 0;
    }
    else if (2 <= hPrime && hPrime < 3) {

      rOne = 0;
      gOne = c;
      bOne = x;
    }
    else if (3 <= hPrime && hPrime < 4) {

      rOne = 0;
      gOne = x;
      bOne = c;
    }
    else if (4 <= hPrime && hPrime < 5) {

      rOne = x;
      gOne = 0;
      bOne = c;
    }
    else {

      rOne = c;
      gOne = 0;
      bOne = x;
    }

    const m = this.v - c;

    const r = rOne + m;
    const g = gOne + m;
    const b = bOne + m;

    return new RGBColor(Math.floor(r * 255), Math.floor(g *255), Math.floor(b*255));
  }
}
