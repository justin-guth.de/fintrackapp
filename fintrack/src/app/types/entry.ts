/* eslint-disable @typescript-eslint/naming-convention */
import { Tag } from './tag';
export interface Entry {

  id?: string;
  title: string;
  description?: string;
  date: string;
  category_id: string;
  amount: number;
  absolute_amount: number;
  amountRepresentation?: string;
  categoryName?: string;
  categoryColor?: string;
  categoryIcon?: string;
  tags: Tag[];
}
