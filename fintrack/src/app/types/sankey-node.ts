import { HiddenListingAreaComponent } from './../components/hidden-listing-area/hidden-listing-area.component';
import { Sankey } from 'src/app/types/sankey';
import { BoxClickHandler } from './box-click-handler';
import { CategoryTreeNode } from './category-tree-node';
import { SankeyNodeBoxPositionInfo } from './sankey-node-box-position-info';
import { TextPaintInstruction } from './text-paint-instruction';

export class SankeyNode {



  public id: string;
  public name: string;
  public color: string;
  // private parentNode: SankeyNode = null;
  private childrenNodes: SankeyNode[] = [];
  private pseudoNodes: SankeyNode[] = [];


  private value: number = 0;
  private hidden: boolean = false;

  private boxClickHandler: BoxClickHandler;
  private hiddenListingArea: HiddenListingAreaComponent;

  private sankey: Sankey;

  constructor(id: string, name: string, color: string, sankey: Sankey, hiddenListingArea: HiddenListingAreaComponent) {

    this.sankey = sankey;
    this.id = id;
    this.name = name;
    this.color = color;
    this.hiddenListingArea = hiddenListingArea;
  }

  public static fromCategoryTreeNode(
    tree: CategoryTreeNode,
    sankey: Sankey,
    hiddenListingArea: HiddenListingAreaComponent
  ): SankeyNode {

    const result: SankeyNode = new SankeyNode(tree.id, tree.name, tree.color, sankey, hiddenListingArea);

    for (const child of tree.children) {
      result.childrenNodes.push(SankeyNode.fromCategoryTreeNode(child, sankey, hiddenListingArea));
    }

    return result;
  }

  refresh() {
    this.sankey.recomputePixelPerAmount();
    this.sankey.paint();
  }

  addPseudoNode(node: SankeyNode) {

    this.pseudoNodes.push(node);
  }

  public countLongestNodeChain(): number {

    let longest: number = 0;

    for (const node of this.allNodes()) {

      const childLength: number = node.countLongestNodeChain();

      if (childLength > longest) {
        longest = childLength;
      }
    }

    return longest + 1;
  }

  public countShortestNodeChain(): number {

    let shortest: number = 0;

    for (const node of this.allNodes()) {

      const childLength: number = node.countShortestNodeChain();

      if (childLength < shortest || shortest === 0) {
        shortest = childLength;
      }
    }

    return shortest + 1;
  }

  hide() {
    this.hiddenListingArea.add(this);
    this.hidden = true;
    this.refresh();
  }

  unhide() {
    this.hidden = false;
    this.refresh();
  }


  public countLeafNodes(
    isParentHidden: boolean
  ): number {

    let result: number = 0;

    for (const node of this.allNodes()) {

      result += node.countLeafNodes(this.hidden || isParentHidden);
    }

    if (this.allNodes().length === 0) {

      if (this.value !== 0 && !(this.hidden || isParentHidden)) {

        return 1;
      }
      else {
        return 0;
      }
    }
    else {

      return result;
    }

  }

  public countLeafValue(
    isParentHidden: boolean
  ): number {

    if (this.hidden || isParentHidden) {

      return 0;
    }

    let result: number = 0;

    for (const node of this.allNodes()) {

      result += node.countLeafValue(this.hidden || isParentHidden);
    }

    if (result === 0) {

      return this.value;
    }
    else {

      return result;
    }
  }


  public paintOnCanvas(
    canvas: HTMLCanvasElement,
    xLow: number,
    xHigh: number,
    yLow: number,
    yHigh: number,
    pixelPerAmount: number,
    boxWidth: number,
    verticalBoxPadding: number,
    textMargin: number,
    reversed: boolean,
    alphaSuffix: string,
    isParentHidden: boolean
  ): SankeyNodeBoxPositionInfo {

    const context: CanvasRenderingContext2D = canvas.getContext('2d');


    // Check if this node is a leaf node:
    if (this.allNodes().length === 0) {

      if (this.value === 0 || this.hidden || isParentHidden) {

        return null;
      }

      // compute box height:
      const boxHeight: number = this.value * pixelPerAmount;
      const boxY: number = yLow;
      let boxX: number = xHigh;

      if (reversed) {
        boxX += boxWidth;
      } else {
        boxX -= boxWidth;
      }




      // paint box on canvas
      context.fillStyle = this.color;
      context.fillRect(boxX, boxY, boxWidth, boxHeight);

      this.boxClickHandler = new BoxClickHandler(
        boxX,
        boxY,
        boxWidth,
        boxHeight,
        this,
        this.hiddenListingArea,
        canvas
      );

      return {
        x: boxX, y: boxY, height: boxHeight, width: boxWidth,
        text: this.name, color: this.color,
        barHeight: boxHeight, textPaintInstructions: [],
        rootY: yLow
      };
    }
    // if this node is a parent node, then:
    else {

      const longestChain: number = this.countLongestNodeChain();
      const leftoverXSpace: number = xHigh - xLow;
      const xSpacePerNode: number = leftoverXSpace / longestChain;
      const newXLow: number = xLow + xSpacePerNode;

      const boxHeight: number = this.countLeafValue(false) * pixelPerAmount;

      let newYLow = yLow;

      const paintedChildBoxes: SankeyNodeBoxPositionInfo[] = [];
      let textPaintInstructions: TextPaintInstruction[] = [];

      for (const node of this.allNodes()) {

        const nodePositionInfo: SankeyNodeBoxPositionInfo = node.paintOnCanvas(
          canvas,
          newXLow,
          xHigh,
          newYLow,
          yHigh,
          pixelPerAmount,
          boxWidth,
          verticalBoxPadding,
          textMargin,
          reversed,
          alphaSuffix,
          this.hidden || isParentHidden);

        if (nodePositionInfo == null) {
          continue;
        }

        paintedChildBoxes.push(nodePositionInfo);
        textPaintInstructions = textPaintInstructions.concat(nodePositionInfo.textPaintInstructions);

        newYLow = nodePositionInfo.rootY + nodePositionInfo.height;

        newYLow += verticalBoxPadding;
      }

      if (paintedChildBoxes.length > 0) {

        newYLow -= verticalBoxPadding;
      }
      else {
        return null;
      }

      const centerHeight: number = (newYLow - yLow) / 2;

      const boxX = xLow + xSpacePerNode - boxWidth;
      const boxY: number = yLow + centerHeight - (boxHeight / 2);

      // paint box on canvas
      context.fillStyle = this.color;
      context.fillRect(boxX, boxY, boxWidth, boxHeight);

      this.boxClickHandler = new BoxClickHandler(
        boxX,
        boxY,
        boxWidth,
        boxHeight,
        this,
        this.hiddenListingArea,
        canvas
      );

      const resultHeight: number = newYLow - yLow;

      let yOffset = 0;

      for (const box of paintedChildBoxes) {

        // paint bezier connection on canvas

        let x = boxX;
        const y = boxY + yOffset;
        const xMid = (box.x + x) / 2;


        let xTo = box.x;

        if (reversed) {
          xTo += boxWidth;
        } else {

          x += boxWidth;
        }

        context.beginPath();
        context.moveTo(x, y);
        context.bezierCurveTo(xMid, y, xMid, box.y, xTo, box.y);
        context.lineTo(xTo, box.y + box.barHeight);
        context.bezierCurveTo(xMid, box.y + box.barHeight, xMid, y + box.barHeight, x, y + box.barHeight);

        yOffset += box.barHeight;

        context.closePath();
        context.fillStyle = box.color + alphaSuffix;
        context.fill();

        // prepare text paint instructions

        context.font = '16px sans-serif';
        const textMeasurement: TextMetrics = context.measureText(box.text);
        const textY = box.y + box.barHeight / 2 + (textMeasurement.actualBoundingBoxAscent + textMeasurement.actualBoundingBoxDescent) / 2;

        let textX = box.x;

        if (reversed) {
          textX = textX + textMargin + boxWidth;
        } else {
          textX = textX - textMargin - textMeasurement.width;
        }
        textPaintInstructions.push({
          text: box.text,
          x: textX,
          y: textY
        });
      }



      return {
        x: boxX, y: boxY, height: resultHeight, width: boxWidth,
        text: this.name, color: this.color,
        barHeight: boxHeight, textPaintInstructions,
        rootY: yLow
      };
    }
  }


  public addChildNode(node: SankeyNode) {

    this.childrenNodes.push(node);
  }

  public tryToAdd(id: string, value: number): boolean {

    if (this.allNodes().length === 0) {
      if (id === this.id) {

        // add to this leaf node.

        this.value += value;
        return true;
      }
    }
    else {

      for (const node of this.allNodes()) {

        if (node.tryToAdd(id, value)) {
          return true;
        }
      }

      if (id === this.id) {

        // create pseudo node leaf with same id and tryToAdd onto that
        // only runs once since otherwise it is caught in the preceding for loop

        const node: SankeyNode = new SankeyNode(this.id, 'Other ' + this.name, '#AAAAAA', this.sankey, this.hiddenListingArea);
        node.tryToAdd(id, value);
        this.addPseudoNode(node);
      }

      return false;
    }
  }

  private allNodes(): SankeyNode[] {

    return this.childrenNodes.concat(this.pseudoNodes);
  }

}
