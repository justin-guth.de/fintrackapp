export interface SankeySettings {

  verticalBorderPadding?: number;
  horizontalBorderPadding?: number;

  verticalBarPadding?: number;
  verticalBarWidth?: number;

  textMargin?: number;
}
