import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { UserLoginGuard as UserLoginGuard } from './guards/user-login.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./components/dashboard/dashboard.module').then( m => m.DashboardPageModule),
    canActivate: [UserLoginGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./components/login/login.module').then( m => m.LoginPageModule),
    canActivate: [UserLoginGuard]

  },
  {
    path: 'logout',
    loadChildren: () => import('./components/logout/logout.module').then( m => m.LogoutPageModule)
  },
  {
    path: 'statistics',
    loadChildren: () => import('./components/statistics/statistics.module').then( m => m.StatisticsPageModule),
    canActivate: [UserLoginGuard]
  },
  {
    path: 'details/:id',
    loadChildren: () => import('./components/details-page/details-page.module').then( m => m.DetailsPagePageModule),
    canActivate: [UserLoginGuard]
  },
  {
    path: 'edit/:id',
    loadChildren: () => import('./pages/edit-entry/edit-entry.module').then( m => m.EditEntryPageModule),
    canActivate: [UserLoginGuard]

  },
  {
    path: 'create-category',
    loadChildren: () => import('./pages/create-category/create-category.module').then( m => m.CreateCategoryPageModule),
    canActivate: [UserLoginGuard]
  },
  {
    path: 'import-csv',
    loadChildren: () => import('./pages/import-csv/import-csv.module').then( m => m.ImportCsvPageModule),
    canActivate: [UserLoginGuard]
  },
  {
    path: 'manage-categories',
    loadChildren: () => import('./pages/manage-categories/manage-categories.module').then( m => m.ManageCategoriesPageModule),
    canActivate: [UserLoginGuard]
  },
  {
    path: 'edit-category/:id',
    loadChildren: () => import('./pages/edit-category/edit-category.module').then( m => m.EditCategoryPageModule),
    canActivate: [UserLoginGuard]

  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
