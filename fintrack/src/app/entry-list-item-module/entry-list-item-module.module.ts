import { TinyChipModule } from './../modules/tiny-chip/tiny-chip.module';
import { AmountNumberDisplayModule } from './../modules/amount-number-display/amount-number-display.module';
import { VerticalColorBarModule } from './../modules/vertical-color-bar/vertical-color-bar.module';
import { DashboardPageRoutingModule } from './../components/dashboard/dashboard-routing.module';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { EntryListItemComponent } from './../components/entry-list-item/entry-list-item.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    EntryListItemComponent
  ],
  exports: [
    EntryListItemComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule,
    VerticalColorBarModule,
    AmountNumberDisplayModule,
    TinyChipModule
  ]
})
export class EntryListItemModuleModule { }
