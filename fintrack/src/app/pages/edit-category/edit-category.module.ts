import { CategoryBoxModule } from './../../modules/category-box/category-box.module';
import { ColorPickerModule } from './../../modules/color-picker/color-picker.module';
import { CollapsibleCardModule } from './../../modules/collapsible-card/collapsible-card.module';
import { IconSelectionModule } from './../../modules/icon-selection/icon-selection.module';
import { CategorySelectionModule } from './../../modules/category-selection/category-selection.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditCategoryPageRoutingModule } from './edit-category-routing.module';

import { EditCategoryPage } from './edit-category.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditCategoryPageRoutingModule,
    CategorySelectionModule,
    CategoryBoxModule,
    IconSelectionModule,
    CollapsibleCardModule,
    ColorPickerModule
  ],
  declarations: [EditCategoryPage]
})
export class EditCategoryPageModule {}
