import { CategoryBoxComponent } from './../../components/category-box/category-box.component';
import { ColorPickerComponent } from './../../components/color-picker/color-picker.component';
import { IconSelectionComponent } from 'src/app/components/icon-selection/icon-selection.component';
import { IonInput, IonButton } from '@ionic/angular';
import { CategorySelectionComponent } from './../../components/category-selection/category-selection.component';
import { Category } from './../../types/category';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from 'src/app/services/category.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.page.html',
  styleUrls: ['./edit-category.page.scss'],
})
export class EditCategoryPage implements OnInit, AfterViewInit {

  @ViewChild('categorySelection') categorySelection: CategorySelectionComponent;
  @ViewChild('colorInput') colorInput: IonInput;
  @ViewChild('colorPicker') colorPicker: ColorPickerComponent;
  @ViewChild('nameInput') nameInput: IonInput;
  @ViewChild('submitButton') submitButton: IonButton;
  @ViewChild('iconSelection') iconSelection: IconSelectionComponent;
  @ViewChild('previewBox') previewBox: CategoryBoxComponent;

  public category: Category;
  constructor(private categoryService: CategoryService,
    private route: ActivatedRoute, private location: Location) { }


  ngOnInit() {

    const id: string = this.route.snapshot.params.id;
    this.categoryService.getCategory(id, (c) => {

      this.category = c;
      this.categorySelection.hide(c.id);
      this.categorySelection.setInitial(c.parent_id);

      this.submitButton.disabled = false;
    });

  }

  ngAfterViewInit(): void {
    this.validateForm();
  }

  onColorPickerValueChange() {

    this.colorInput.value = this.colorPicker.getColor();
    this.previewBox.setColor(this.colorPicker.getColor());
  }

  validateForm() {


    if (this.nameInput === undefined || this.colorInput === undefined) {

      this.submitButton.disabled = true;
    }
    else if (this.nameInput.value === '' || this.nameInput.value === null || this.nameInput.value === undefined) {

      this.submitButton.disabled = true;
    }
    else if (this.colorInput.value === null
      || this.colorInput.value === undefined
      || !(this.colorInput.value as string).match(/#(\d|[A-F]|[a-f]){6}$/)) {

      this.submitButton.disabled = true;
    }
    else {

      this.submitButton.disabled = false;
    }

    if ((this.colorInput.value as string).match(/#(\d|[A-F]|[a-f]){6}$/)) {

      this.colorPicker.setColor(this.colorInput.value as string);
      this.previewBox.setColor(this.colorInput.value as string);
    }
  }

  submit() {

    const id = this.category.id;
    const name = this.nameInput.value as string;
    const color = this.colorInput.value as string;
    const parentId = this.categorySelection.getSelection();
    const icon = this.iconSelection.value;

    this.categoryService.modifyCategory(id, name, color, parentId, icon, (result: Category) => {

      this.location.back();
    });
  }

  selectIcon() {

    this.previewBox.setIcon(this.iconSelection.value);

  }
}
