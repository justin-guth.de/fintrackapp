import { ColorPickerModule } from './../../modules/color-picker/color-picker.module';
import { ColorPickerComponent } from './../../components/color-picker/color-picker.component';
import { CategorySelectionModule } from './../../modules/category-selection/category-selection.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateCategoryPageRoutingModule } from './create-category-routing.module';

import { CreateCategoryPage } from './create-category.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateCategoryPageRoutingModule,
    CategorySelectionModule,
    ColorPickerModule
  ],
  declarations: [CreateCategoryPage]
})
export class CreateCategoryPageModule {}
