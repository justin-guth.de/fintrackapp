import { ColorPickerComponent } from './../../components/color-picker/color-picker.component';
import { CategoryService } from 'src/app/services/category.service';
import { CategorySelectionComponent } from './../../components/category-selection/category-selection.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInput } from '@ionic/angular';
import { Location } from '@angular/common';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.page.html',
  styleUrls: ['./create-category.page.scss'],
})
export class CreateCategoryPage implements OnInit {

  @ViewChild('nameInput') nameInput: IonInput;
  @ViewChild('colorInput') colorInput: ColorPickerComponent;
  @ViewChild('categorySelection') categorySelection: CategorySelectionComponent;

  constructor(private categoryService: CategoryService,
    private location: Location) { }

  ngOnInit() {
  }

  create() {

    this.categoryService.createCategory({

      name: this.nameInput.value as string,
      color: this.colorInput.getColor(),
      // eslint-disable-next-line @typescript-eslint/naming-convention
      parent_id: this.categorySelection.getSelection(),
    },
    (created) => {

      console.log('created:', created);
      this.location.back();

    });
  }

}
