import { EntryService } from './../../services/entry.service';
import { Router } from '@angular/router';
import { CategoryService } from 'src/app/services/category.service';
import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/types/category';

@Component({
  selector: 'app-manage-categories',
  templateUrl: './manage-categories.page.html',
  styleUrls: ['./manage-categories.page.scss'],
})
export class ManageCategoriesPage implements OnInit {

  public categories: Category[];

  constructor(private categoryService: CategoryService, private router: Router, private entryService: EntryService) { }

  ngOnInit() {


    this.refresh();


    this.entryService.addOnChangeCallback(() => {
      this.refresh();

    });
  }

  refresh() {
    this.categoryService.getCategories((cats: Category[]) => {

      this.categories = cats;
    });
  }

  editCategory(id: string) {

    this.router.navigate(['edit-category/' + id]);
  }

  navigateToCategoryCreation() {

    this.router.navigate(['create-category']);

  }

}
