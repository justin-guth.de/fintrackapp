import { IonButton, LoadingController, ToastController } from '@ionic/angular';
import { Entry } from './../../types/entry';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ImportCsvService } from 'src/app/services/import-csv.service';

@Component({
  selector: 'app-import-csv',
  templateUrl: './import-csv.page.html',
  styleUrls: ['./import-csv.page.scss'],
})
export class ImportCsvPage implements OnInit {

  @ViewChild('importButton') importButton: IonButton;

  public file: File;
  constructor(
    private importCsvService: ImportCsvService,
    private loadingController: LoadingController,
    private toastController: ToastController
  ) { }

  ngOnInit() {
  }

  openFile() {
    (document.querySelector('#fileInput') as HTMLInputElement).click();
  }

  handle(e) {
    this.file = (document.querySelector('#fileInput') as HTMLInputElement).files[0];
    this.importButton.disabled = false;
  }

  import() {

    this.importButton.disabled = true;
    const loading = this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
    });

    const toast = this.toastController.create({
      message: 'Entries have been imported.',
      duration: 2000
    });

    toast.then((t) => t.present());
    loading.then((e) => e.present());

    this.file.text().then(
      (content: string) => {

        console.log('Selecting file:', content);

        this.importCsvService.importCsv(content, (results: Entry[]) => {

          this.file = null;
          //this.importButton.disabled = false;
          loading.then((e) => e.dismiss());
        });
      }
    );
  }
}
