import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImportCsvPage } from './import-csv.page';

const routes: Routes = [
  {
    path: '',
    component: ImportCsvPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImportCsvPageRoutingModule {}
