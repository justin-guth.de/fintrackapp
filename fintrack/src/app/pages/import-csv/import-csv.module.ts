import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImportCsvPageRoutingModule } from './import-csv-routing.module';

import { ImportCsvPage } from './import-csv.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImportCsvPageRoutingModule
  ],
  declarations: [ImportCsvPage]
})
export class ImportCsvPageModule {}
