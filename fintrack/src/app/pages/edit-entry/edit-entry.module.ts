import { CategorySelectionModule } from './../../modules/category-selection/category-selection.module';
import { AmountInputModule } from '../../modules/amount-input/amount-input.module';
import { EditEntryComponent } from './../../components/edit-entry/edit-entry.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditEntryPageRoutingModule } from './edit-entry-routing.module';

import { EditEntryPage } from './edit-entry.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditEntryPageRoutingModule,
    AmountInputModule,
    CategorySelectionModule
  ],
  declarations: [EditEntryPage, EditEntryComponent]
})
export class EditEntryPageModule {}
