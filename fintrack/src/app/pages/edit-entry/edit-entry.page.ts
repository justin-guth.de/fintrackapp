import { EditEntryComponent } from './../../components/edit-entry/edit-entry.component';
import { Entry } from './../../types/entry';
import { ActivatedRoute } from '@angular/router';
import { EntryService } from './../../services/entry.service';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-edit-entry-page',
  templateUrl: './edit-entry.page.html',
  styleUrls: ['./edit-entry.page.scss'],
})
export class EditEntryPage implements OnInit {

  @ViewChild('editComponent') editComponent: EditEntryComponent;

  public entry: Entry;

  constructor(private entryService: EntryService, private route: ActivatedRoute) { }

  ngOnInit() {

    const id: string = this.route.snapshot.params.id;
    this.entryService.getEntry(id, (entry) => {

      this.entry = entry;
    });
  }

}
