import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'fintrack',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
